
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    connect = require('gulp-connect'),
    stylus = require('gulp-stylus'),
    nib = require('nib'),
    gulpif = require('gulp-if');
    minifyCss = require('gulp-minify-css');
    useref = require('gulp-useref');


// Preprocesa archivos Stylus a CSS y recarga los cambios
gulp.task('css', function() {
 gulp.src('./public/css/module/front/*.styl')
 	.pipe(stylus({ use: nib() }))
 	.pipe(gulp.dest('./public/css/module/front'))
 	.pipe(connect.reload());
});


//Automatizamos esta tarea
gulp.task('watch', function(){
    gulp.watch(['./public/css/module/front/*.styl'], ['css']);

});

//ejecutamos el servidor y todos los archivos
gulp.task('default', ['watch','css']);
