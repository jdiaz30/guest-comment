$(document).ready(function(){

     listComment(true);


	$("#idioma").change(function(){
		var vIndex = $(this).get(0).selectedIndex;
		var vValue = $(this).get(0).options[vIndex].value;

		if(vIndex !=0){
			listComment(false);
		}
		
	})
});

function verPromedio(){
	var booking = $("#booking_url").val();
	var despegar = $("#despegar_url").val();
	var orbitz = $("#orbitz_url").val();
	var hoteles = $("#hoteles_url").val();
	var expedia = $("#expedia_url").val();

	_Booking = "booking="+booking;
	_Despegar = "despegar="+despegar;
	_Orbitz = "orbitz="+orbitz;
	_Hoteles = "hotels="+hoteles;
	_Expedia = "expedia="+expedia;

	var vDatos = _Booking+"&"+_Despegar+"&"+_Hoteles+"&"+_Orbitz+"&"+_Expedia

	$.ajax({
		url:"/hotel/promedio-comment",
		type:'POST',
		async:true,
		data:vDatos,
		beforeSend:function(){

		},
		success:function(respuesta){
				
		}
	});
}

function verMas(vId){
	if( $('#'+vId).is(":visible") ){
		$("#"+vId).fadeOut(180);
		$("#a"+vId).html("View more");

	}else{
		$("#"+vId).fadeIn(180);
		$("#a"+vId).html("-");
	}

}
function capturaIdioma(){
		var vIndex = $("#idioma").get(0).selectedIndex;
		var vValue = $("#idioma").get(0).options[vIndex].value;

		if(vIndex == 0){
			vValue ="es_ES";
		}

		return vValue;
}

function listComment(vPromedio){
	var url = $("#url").val();
	var id = $("#hotel-id").val();
	var idioma = capturaIdioma();

	_Url = "url="+url;
	_Id = "hotel-id="+id;
	_Idioma = "idioma="+idioma;

	var vDatos = _Url+"&"+_Id+"&"+_Idioma;

	$.ajax({
		url:"/hotel/list-comment",
		type:'POST',
		async:true,
		data: vDatos,
		beforeSend:function(){

		if($(".comment-content").is(":visible")){
        	$(".comment-content").fadeOut(100,function(){
        		$(".loading").fadeIn(100);
        	});
			}else{
				$(".loading").fadeIn(100);
			}

		},
		success:function(respuesta){

			if($(".comment-content").is(":visible")){

				$(".loading").fadeOut(100,function(){
					 		$(".comment-content").html(respuesta);
				})
			}else{

				$(".loading").fadeOut(100,function(){
					if(vPromedio==true){
					   verPromedio();
					}
					
					$(".comment-content").html(respuesta);
					$(".comment-content").fadeIn(100);
				})
			}


			}
	});
}

function masComentarios(vPaginador,vLimit){

	var id = $("#hotel-id").val();
	var idioma = capturaIdioma();

	_Paginador="paginador="+vPaginador;
	_Limit="limit="+vLimit;
	_Id = "hotel-id="+id;
	_Idioma = "idioma="+idioma;

	var vDatos=_Paginador+"&"+_Limit+"&"+_Id+"&"+_Idioma;

	$.ajax({
		url:"/hotel/list-comment-paginador",
		type:'POST',
		async:true,
		data:vDatos,
		beforeSend:function(){
			$(".more-comment a").fadeOut(100,function(){
        			$(".loading-c").fadeIn(100)
			})

		},
		success:function(respuesta){

				$(".more-comment").remove();

				$(".comment-content").append(respuesta);

			}
	});
}

function cambiarIdioma(vIdioma,vPromedio){
	$.ajax({
		url:"/change-language",
		type:'POST',
		async:true,
		data:"language="+vIdioma,

		success:function(respuesta){
			listComment(vPromedio);
		}
	});
}
