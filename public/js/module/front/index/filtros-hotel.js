$(function() {



	var vFiltro = {
		init : function(){
			vFiltro.filtrar();

      var vData = $("#txtBuscar").val();

			$("#btn-search-home").click(function(){
          vFiltro.search(vData);
      });

      /*$("#txtBuscar").keypress(function(e) {
          if(e.which == 13) {
            vFiltro.search(vData);
          }
      });*/

      $('input.typeahead').on('typeahead:selected', function(event, selection) {
         vFiltro.search(selection.value);
         //alert(selection.id);
      });

		},
		filtrar : function(){
 
       		var paisData = new Bloodhound({
          	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
          	queryTokenizer: Bloodhound.tokenizers.whitespace,
          	limit:2,
            prefetch: {
               ttl: 1,
               url:"/data/data-pais.json",
             
            }
      		});

          var ciudadData = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit:3,
            prefetch: {
              ttl: 1,
              url:"/data/data-ciudad.json",
              filter: function(list) {
                  return $.map(list, function(data) { 
                    return { ciudad:data.ciudad+", "+data.pais ,value: data.ciudad, pais: data.pais,id:'ciudad'};
                  });
    
              }
              }
            
          });

          var localidadData = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit:3,

            prefetch: {
              ttl: 1,
              url:"/data/data-localidad.json",
          
              filter: function(list) {
                  return $.map(list, function(data) { 
                    return {name:data.localidad+", "+data.ciudad+", "+data.pais, value: data.localidad,ciudad: data.ciudad,pais: data.pais,id:'localidad'};

                  });
    
              }
            }
             
          });

          var hotelData = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit:4,
            prefetch: {
            ttl: 1,
            url: "/data/data-hotel.json",
            filter: function(list) {
                  return $.map(list, function(hotel) { 
                    return {name:hotel.nombre+", "+hotel.ciudad+", "+hotel.pais, value: hotel.nombre,ciudad: hotel.ciudad,pais:hotel.pais,id:'hotel'};

                  });
    
              }
              }
            
          });
 
     		
     		paisData.initialize();
        localidadData.initialize();
        ciudadData.initialize();
        hotelData.initialize();

     		$('.typeahead').typeahead( {
          		hint: true,
          		highlight: true,
          		minLength: 1,
          		autoselect: true,
      		},
          {
            name: 'pais-data',
              displayKey: 'value',
              source: paisData.ttAdapter(), 
          },
          {
            name: 'ciudad-data',
              displayKey: 'ciudad',
              source: ciudadData.ttAdapter(),
              templates: {
                  suggestion: Handlebars.compile('{{value}}, {{pais}}')
              }
          },
          {
            name: 'localidad-data',
              displayKey: 'name',
              source: localidadData.ttAdapter(),
              templates: {
                  suggestion: Handlebars.compile('{{value}}, {{ciudad}}, {{pais}}')
              }
          },
      		{
      			name: 'hotel-data',
          		displayKey: 'name',
          		source: hotelData.ttAdapter(),
              templates: {
                  suggestion: Handlebars.compile('{{value}}, {{ciudad}}, {{pais}}'),
              }
      		}
      		

      		);

  	 	},
  	 	search : function(vData){
  	 		if(vData!=""){
				var urlId = vFiltro.returnString(vData);
        		urlId = vFiltro.replaceAll(urlId,"/", "");
        		var url = "/search/"+urlId
        		window.location.href = url;
  	 		}else{
  	 			$("#txtBuscar").addClass('error');
  	 		}
        

    	},
    	returnString : function(val) {
        	val = val.replace(/-+/g,' ');
        	val = val.replace(/_+/g,' ');
        	//val = val.replace(/\.+/g,'');     
        	val = val.replace(/\s/g,'+');
        	//val = val.replace(/,+/g,'');
        	val = val.replace(/\%+/g,' ');
        	return val;
    	},
    	replaceAll : function (str, str1, str2){
        	var index = str.indexOf( str1 );
        	while (index != -1){
          		str = str.replace(str1, str2);
          		index = str.indexOf(str1);
        	}
          	return str;
        }
	}

	vFiltro.init();
});


	