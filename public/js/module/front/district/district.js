$(function() {
	var vSearch = {
		init : function(){
			vSearch.searchInit();

			$("#ord_estrella").change(function(){
				//$("#ord_puntaje").get(0).selectedIndex=0;
				vSearch.searchInit();
			});

			$("#ord_puntaje").change(function(){
				//$("#ord_estrella").get(0).selectedIndex=0;
				vSearch.searchInit();
			});

		
			$('#modalMapa').on('hidden.bs.modal', function (e) {
  				$("#mapa_data").html("");
			})


		},

		searchInit : function(){
			var vEstrellas = vSearch.capturaDataCombo("ord_estrella");
			var vPuntaje = vSearch.capturaDataCombo("ord_puntaje");
			var vDistrict = $("#texto").val();

			_Estrella = "estrellas="+vEstrellas;
			_Puntaje = "puntaje="+vPuntaje;
			_District = "localidad="+vDistrict;

			var vDatos = _Estrella+"&"+_Puntaje+"&"+_District;

			$.ajax({
				url:"/district/search-hotel",
				type:'POST',
				async:true,
				data:vDatos,
				beforeSend:function(){
					if($("#container-hotel-search").is(":visible")){
						$("#container-hotel-search").fadeOut(100,function(){
							$("#container-hotel-search").html("");

							$(".loading").fadeIn(100);
						});
					}else{
						$(".loading").fadeIn(100);
					}
				},
				success:function(respuesta){

					if($("#container-hotel-search").is(":visible")){

						$(".loading").fadeOut(100,function(){
					 		$("#container-hotel-search").html(respuesta);
					 		vSearchMotor.verMapa();
							vSearchMotor.dateRangePicker();
							vSearchMotor.focusText();
							vSearchMotor.calcularDisponibilidad();
				    	});
					}else{

					 	$(".loading").fadeOut(100,function(){
					 	
					 		$("#container-hotel-search").html(respuesta);
					 		$("#container-hotel-search").fadeIn(300);
					 		vSearchMotor.verMapa();
							vSearchMotor.dateRangePicker();
							vSearchMotor.focusText();
							vSearchMotor.calcularDisponibilidad();
				   		});
					}

			}
		});

		},
		capturaDataCombo : function(vId){
			var vIndex = $("#"+vId).get(0).selectedIndex;
			var vValue = $("#"+vId).get(0).options[vIndex].value;

			if(vIndex == 0){
				vValue = "";
			}
			return vValue;
		}

	}

	vSearch.init();

});