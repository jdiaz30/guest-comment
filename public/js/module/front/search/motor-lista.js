$(function() {
  vSearchMotor = {
    dateRangePicker : function(){
      initializeBitCal();
      $(".box-calendar .beatpicker-inputnode-from").attr("placeholder","Ingreso");
      $(".box-calendar .beatpicker-inputnode-to").attr("placeholder","Salida");
    },
    focusText : function(){
     $(".box-calendar .beatpicker-inputnode-from").click(function(){
      var vItem = $(this).attr("item")-1;
      if($("a.btn-precio").eq(vItem).is(":hidden")){
         $(".box-calendar:eq("+vItem+") .box-precios").remove();
         $("a.btn-precio").eq(vItem).fadeIn(30);
      }

    })


    $(".box-calendar .beatpicker-inputnode-to").click(function(){
      var vItem = $(this).parent().find(".beatpicker-inputnode-from").attr("item")-1;
      if($("a.btn-precio").eq(vItem).is(":hidden")){
         $(".box-calendar:eq("+vItem+") .box-precios").remove();
         $("a.btn-precio").eq(vItem).fadeIn(30);
      }

    })
   },
   calcularDisponibilidad : function(){

    $("a.btn-precio").click(function(){
       var vItem = $(this).attr("item")-1;

       var vFechaIng = $(".box-calendar .beatpicker-inputnode-from").eq(vItem).val();
       var vFechaSal = $(".box-calendar .beatpicker-inputnode-to").eq(vItem).val();

       $(".box-calendar").each(function(){
          
          var vBooking = $(this).find("a.btn-precio").attr("booking");
          var vHoteles = $(this).find("a.btn-precio").attr("hoteles");
          var vExpedia = $(this).find("a.btn-precio").attr("expedia");
          var vOrbitz = $(this).find("a.btn-precio").attr("orbitz");
        
          $(this).find(".beatpicker-inputnode-from").val(vFechaIng);
          $(this).find(".beatpicker-inputnode-to").val(vFechaSal);

          if(vFechaSal!="" && vFechaIng!=""){

              $(this).find("a.btn-precio").fadeOut(10,function(){

                _FechaIni = "fecha-ini="+vFechaIng;
                _FechaSal = "fecha-sal="+vFechaSal;
                _Booking = "booking="+vBooking;
                _Hoteles = "hoteles="+vHoteles;
                _Expedia = "expedia="+vExpedia;
                _Orbitz  = "orbitz="+vOrbitz;

                var vData = _Booking+"&"+_Hoteles+"&"+_Expedia+"&"+_Orbitz+"&"+_FechaIni+"&"+_FechaSal;

                vSearchMotor.calcular(vData,this);
                


              });

          }
         
       })


   })
   }
   ,
   calcular : function(vData,vId){
      if($(vId).parent().find(".box-precios").is(":visible")){
         $(vId).parent().find(".box-precios").remove();
      }

      $.ajax({
          url:"/hotel/motor-reserva",
          type:'POST',
          async:true,
          data:vData,
          beforeSend:function(){
            
              $(vId).parent().append("<div class='center'><img src='/img/loading-tr.gif' width='40'></div>");
          },
          success:function(respuesta){
              $(vId).parent().find(".center").remove();
              $(vId).parent().append("<div class='box-precios' style='display:block'>"+respuesta+"</div>");
          }
      }); 
   },
   calcularDisponibilidadOriginal : function(){
     
    /*var elems = $("*").filter(function () {
        return $(this).data("beatpicker")
    });

    $(elems).each(function () {
        var elem = $(this);
        var id = elem.attr("data-beatpicker-id");
        id.on("select", function (data) {
          alert("dsds");
        })

    });
    */
    $("a.btn-precio").click(function(){
        var vItem = $(this).attr("item") -1;
        var vBooking = $(this).attr("booking");
        var vHoteles = $(this).attr("hoteles");
        var vExpedia = $(this).attr("expedia");
        var vOrbitz = $(this).attr("orbitz");
        var vFechaIng = $(".box-calendar .beatpicker-inputnode-from").eq(vItem).val();
        var vFechaSal = $(".box-calendar .beatpicker-inputnode-to").eq(vItem).val();

        if(vFechaSal!="" && vFechaIng!=""){

          $(this).fadeOut(200,function(){

            _FechaIni = "fecha-ini="+vFechaIng;
            _FechaSal = "fecha-sal="+vFechaSal;
            _Booking = "booking="+vBooking;
            _Hoteles = "hoteles="+vHoteles;
            _Expedia = "expedia="+vExpedia;
            _Orbitz  = "orbitz="+vOrbitz;

            var vData = _Booking+"&"+_Hoteles+"&"+_Expedia+"&"+_Orbitz+"&"+_FechaIni+"&"+_FechaSal;

            $.ajax({
              url:"/hotel/motor-reserva",
              type:'POST',
              async:true,
              data:vData,
              beforeSend:function(){
                  $(".box-calendar").eq(vItem).append("<div class='center'><img src='/img/loading-tr.gif' width='40'></div>");

              },
              success:function(respuesta){
                  $(".box-calendar:eq("+vItem+") .center").remove();
                  $(".box-calendar").eq(vItem).append("<div class='box-precios'>"+respuesta+"</div>");
              }
            });  
          });

        }

   })
},
verMapa : function(){
  $('a.mapaH').click(function() {
              //vSearch.verMapa();
              var vLongitud = $(this).attr("data-longitud");
              var vLatitud = $(this).attr("data-latitud");
              var vZoom = $(this).attr("data-zoom");
              var vName = $(this).attr("data-name");

              $("#modalMapa").modal("show");

              setTimeout(function(){
                if(vLongitud!=""){
                  var map = new GMaps({
                    div: '#mapa_data',
                    lat: vLatitud,
                    lng: vLongitud,
                    width:'100%',
                    height:"400px",
                    zoom: parseInt(vZoom)      

                  });

                  map.addMarker({
                    lat: vLatitud,
                    lng: vLongitud,
                    title: vName,
                    infoWindow: {
                      content: '<p>'+vName+'</p>'
                    },
                    icon : '/img/hotel-f2.png',
                    animation: google.maps.Animation.BOUNCE,


                  });

                }

              },500);


            });
}

}



});