$(function() {
	var vSearch = {
		init : function(){
			vSearch.searchInit();

			$("#ord_estrella").change(function(){
				$("#ord_puntaje").get(0).selectedIndex=0;
				vSearch.filtrarHotel();
			});

			$("#ord_puntaje").change(function(){
				$("#ord_estrella").get(0).selectedIndex=0;
				vSearch.filtrarHotel();
			});

			$('#modalMapa').on('hidden.bs.modal', function (e) {
  				$("#mapa_data").html("");
			})

		},
		searchInit : function(){
			var vTexto = $("#texto").val();
			var vParam = $("#param").val();

			_Texto = "texto="+vTexto;
			_Param = "param="+vParam;

			var vDatos =_Param+"&"+_Texto;

			$.ajax({
				url:"/search/search-hotel",
				type:'POST',
				async:true,
				data:vDatos,
				beforeSend:function(){
						$(".loading").fadeIn(100);
				},
				success:function(respuesta){

				$(".loading").fadeOut(100,function(){

					$("#container-hotel-search").html(respuesta);

					vSearchMotor.verMapa();
					vSearchMotor.dateRangePicker();
					vSearchMotor.focusText();
					vSearchMotor.calcularDisponibilidad();

				});

			}
		});

		},
        
		filtrarHotel : function(){
			var vParam = $("#param").val();
			var vEstrella = vSearch.capturaDataCombo("ord_estrella");
			var vPuntaje = vSearch.capturaDataCombo("ord_puntaje");

			_Param = "param="+vParam;
			_Estrella = "estrella="+vEstrella;
			_Puntaje = "puntaje="+vPuntaje;

			var vDatos = _Param +"&"+_Estrella+"&"+_Puntaje;
			
			$.ajax({
				url:"/search/filtrar-hotel",
				type:'POST',
				async:true,
				data:vDatos,
				beforeSend:function(){
				
					if($("#container-hotel-search").is(":visible")){
						$("#container-hotel-search").fadeOut(100,function(){
							$("#container-hotel-search").html("");

							$(".loading").fadeIn(100);
						});
					}else{
						$(".loading").fadeIn(100);
					}	

				},
				success:function(respuesta){

					if($("#container-hotel-search").is(":visible")){

						$(".loading").fadeOut(100,function(){
					 		$("#container-hotel-search").html(respuesta);
					 		vSearchMotor.verMapa();
					 		vSearchMotor.dateRangePicker();
					 		vSearchMotor.focusText();
					 		vSearchMotor.calcularDisponibilidad();
					 	
				    	});
					}else{

					 	$(".loading").fadeOut(100,function(){
					 	
					 		$("#container-hotel-search").html(respuesta);
					 		$("#container-hotel-search").fadeIn(300);
					 		vSearchMotor.verMapa();
					 		vSearchMotor.dateRangePicker();
					 		vSearchMotor.focusText();
					 		vSearchMotor.calcularDisponibilidad();
					 		
				   		});
					}

				}
			});


		},
		capturaDataCombo : function(vId){
			var vIndex = $("#"+vId).get(0).selectedIndex;
			var vValue = $("#"+vId).get(0).options[vIndex].value;

			if(vIndex == 0){
				vValue = "";
			}
			return vValue;
		}

	}

	vSearch.init();

});