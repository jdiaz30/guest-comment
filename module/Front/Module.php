<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Front;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Front\Model\HotelTable;
use Front\Model\FotoTable;
use Front\Model\CiudadTable;
use Front\Model\FotoHotelTable;
use Front\View\Helper\ListarHoteles;


class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        //Sirve para habilitar los logs

		/*$application   = $e->getApplication();
        $sm = $application->getServiceManager();
    	$sharedManager = $application->getEventManager()->getSharedManager();

    	$sharedManager->attach('Zend\Mvc\Application', 'dispatch.error',function($e) use ($sm) {
            if ($e->getParam('exception')){
                $sm->get('Zend\Log\Logger')->crit($e->getParam('exception'));
            }
        }
        );*/

        //Conseguimos el servicio traductor
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        //Cargamos la sesión idioma
        $languageSession = new Container('language');   
        //Asignamos el idioma en sesión
        $translator->setLocale( $languageSession->language)->setFallbackLocale( $languageSession->language);

        //Cargamos algunas librerias
        include_once( APPLICATION_PATH . "/../vendor/Dom/simple_html_dom.php");
        include_once( APPLICATION_PATH . "/../vendor/EpicCurl/epicCurl.php");
  
    }

    public function getServiceConfig() {
      return array(
        'factories' => array(
            'Front\Model\HotelTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new HotelTable($dbAdapter);
                return $table;
            },
            'Front\Model\FotoTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new FotoTable($dbAdapter);
                return $table;
            },
            'Front\Model\FotoHotelTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new FotoHotelTable($dbAdapter);
                return $table;
            },
            'Front\Model\CiudadTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new CiudadTable($dbAdapter);
                return $table;
            }
            )
        );
    }

    /*public function getViewHelperConfig() {
        return array(
            'factories' => array(
                'ListarHoteles' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new ListarHoteles($serviceLocator);
             
                }
            ),
        );
    }*/


 
}
