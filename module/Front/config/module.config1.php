<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Front\Controller\Index' => 'Front\Controller\IndexController',
            'Front\Controller\Hotel' => 'Front\Controller\HotelController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Front\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[:controller[/:action]][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(

                            ),
                        ),
                    ),

                    'hotel' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/hotel',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                               
                            ),
                            'defaults' => array(
                                'controller' => 'Front\Controller\Hotel',
                                'action' => 'index',
                            ),
                        ),
                    ),

                    /*'hotel' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[:controller[/:url]][/]',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'hotel',
                                'action' => 'detalle',
                            ),
                        ),
                    ),*/

                    /*'comment' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'list-comment',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action' => 'list-comment',
                            ),
                        ),
                    ),
                    'paginador' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'list-comment-paginador',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action' => 'list-comment-paginador',
                            ),
                        ),
                    ),

                    'promedio' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'promedio-comment',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action' => 'promedio-comment',
                            ),
                        ),
                    ),

                    'language' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'change-language',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action' => 'change-language',
                            ),
                        ),
                    ),*/

                ),
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error-f/404',
        'exception_template' => 'error-f/index',
        'template_map' => array(
            'front/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error-f/404' => __DIR__ . '/../view/error/404.phtml',
            'error-f/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            'front' => __DIR__ . '/../view',
        ),
    ),
     //Agregar plugins
    'controller_plugins' => array(
        'invokables' => array(
            'comentarioPlugin' => 'Front\Plugin\ComentarioPlugin',

        )
    ),

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),

   'translator' => array(
        'locale' => 'es_ES',
        'translation_file_patterns' => array(
            array(
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ),
        ),
    ),

);
