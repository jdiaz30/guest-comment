<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(

    'console' => array(
        'router' => array(
        'routes' => array(
                    //CRON RESULTS SCRAPER
                    'my-first-route' => array(
                    'type'    => 'simple',       // <- simple route is created by default, we can skip that
                    'options' => array(
                    'route'    => 'hello',
                    'defaults' => array(
                        'controller' => 'HotelCronJobController',
                        'action'     => 'index'
                        )
                    )
                )

                ),
                ),
            ),

/*'controllers' => array(
        'invokables' => array(
           
            'Front\Controller\HotelCronJob' => 'Front\Controller\HotelCronJobController',
        ),
    ),*/

    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                       /* 'controller' => 'Front\Controller\Index',
                        'action'     => 'index',*/
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action

       

            'front' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Front\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[:controller][/:action][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',

                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'hotel-url' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'hotel/name[/:name][/]',
                            'constraints' => array(
                              
                            ),
                            'defaults' => array(
                                'controller' => 'hotel',
                                'action' => 'index',
                            ),
                        ),
                    ),

                    'buscar-home' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'search[/:p]',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'search',
                                'action' => 'index',
                            ),
                        ),
                    ),

                    'buscar-hotel' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'search/search-hotel',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'search',
                                'action' => 'search-hotel',
                            ),
                        ),
                    ),
                    'buscar-hotel-filtros' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'search/filtrar-hotel',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'search',
                                'action' => 'filtrar-hotel',
                            ),
                        ),
                    ),
                    'hotel-filtros' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'filtrar-hotel',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action' => 'filtrar-hotel',
                            ),
                        ),
                    ),

                    'language' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'change-language',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action' => 'change-language',
                            ),
                        ),
                    ),

                    'country' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'country[/:action][/:country][/:id][/]',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'country',
                                'action' => 'index',
                            ),
                        ),
                    ),

               
                    'city' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'city[/:action][/:city][/:id][/]',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'city',
                                'action' => 'index',
                            ),
                        ),
                    ), 
                    'district' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'district[/:action][/:district][/]',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'district',
                                'action' => 'index',
                            ),
                        ),
                    ), 


                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Front\Controller\Index' => 'Front\Controller\IndexController',
            'Front\Controller\Hotel' => 'Front\Controller\HotelController',
            'Front\Controller\Search' => 'Front\Controller\SearchController',
            'Front\Controller\Country' => 'Front\Controller\CountryController',
            'Front\Controller\City' => 'Front\Controller\CityController',
            'Front\Controller\District' => 'Front\Controller\DistrictController',
            'Front\Controller\DataJson' => 'Front\Controller\DataJsonController',
            'Front\Controller\HotelCronJob' => 'Front\Controller\HotelCronJobController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error-f/404',
        'exception_template' => 'error-f/index',
        'template_map' => array(
            'front/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error-f/404' => __DIR__ . '/../view/error/404.phtml',
            'error-f/index' => __DIR__ . '/../view/error/index.phtml',
            'header' => __DIR__.'/../view/layout/partial/header.phtml',
            'footer' => __DIR__.'/../view/layout/partial/footer.phtml',
        ),
        'template_path_stack' => array(
            'front' => __DIR__ . '/../view',
        ),
    ),
     //Agregar plugins
    'controller_plugins' => array(
        'invokables' => array(
            'comentarioPlugin' => 'Front\Plugin\ComentarioPlugin',
            'mapaPlugin' => 'Front\Plugin\MapaPlugin',
            'motorReservasPlugin' => 'Front\Plugin\MotorReservasPlugin',
            'promedioPlugin' => 'Front\Plugin\PromedioPlugin',
            'dataPlugin' => 'Front\Plugin\DataPlugin',
            'curlPlugin' => 'Front\Plugin\CurlPlugin',
        )
    ),

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),

   'translator' => array(
        'locale' => 'es_ES',
        'translation_file_patterns' => array(
            array(
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ),
        ),
    ),

 
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
