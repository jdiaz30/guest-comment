<?php

namespace Front\Plugin; 

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;

class PromedioPlugin extends AbstractPlugin {

    public function calculaPromedio($data){
        $bookingPuntaje = 0;
        $despegarPuntaje = 0;
        $expediaPuntaje = 0;
        $hotelsPuntaje = 0;
        $orbitzPuntaje = 0;

    	if($data['booking']!=""){
    		$bookingPuntaje = $this->capturaBooking($data['booking']);
    	}

    	if($data['expedia']!=""){
    		$expediaPuntaje = $this->capturaExpedia($data['expedia']);
    	}
        
    	if($data['despegar']!=""){
    		$despegarPuntaje = $this->capturaDespegar($data['despegar']);
    	}

    	if($data['hotels']!=""){
    		$hotelsPuntaje = $this->capturaHotels($data['hotels']);
    	}

    	if($data['orbitz']!=""){
    		$orbitzPuntaje = $this->capturaOrbitz($data['orbitz']);
    	}

    	$puntaje = $bookingPuntaje + $expediaPuntaje + $despegarPuntaje + $orbitzPuntaje + $hotelsPuntaje;
     
        $promedio = $puntaje / 5;

        $promedio = number_format($promedio, 1, ',', ' '); 

        return $promedio;
    }

    public function capturaBooking($url){

    	$html = $this->curlUrl($url);

        $puntaje = $html->find('#review_list_main_score',0)->plaintext;
       
        return $puntaje;
    }

    public function capturaDespegar($url){

    	$html = $this->curlUrl($url);

        $puntaje = 0;

        if($html->find('.ux-hotels-detail-rate-score')){

            $puntaje = $html->find('.ux-hotels-detail-rate-score',0)->plaintext;

            $puntaje = str_replace("puntos","",$puntaje);

        }else{

            $i = 0;
            foreach ($html->find('div[id=common-comments-list]') as $d):
            foreach ($d->find('.ux-common-comment-bubble') as $e):
                if ($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-best', 0)
                        || $e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-worst', 0)
                        || $e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-description', 0)) {

                    $i+=1;
                    $puntaje+= str_replace("puntos", "", $e->find('.ux-common-comment-bubble-rate', 0)->plaintext);

                }
    
            endforeach;

            endforeach;

            $puntaje = $puntaje / $i;

        }
       
        return $puntaje;
    }

    public function capturaExpedia($url){

        $html = $this->curlUrl($url);

        $puntaje = $html->find('.rating-number',0)->plaintext;
        $puntaje = $puntaje * 2;
       
        return $puntaje;
    }

    public function capturaHotels($url){

        $html = $this->curlUrl($url);

        $puntaje = $html->find('#hotel-reviews-summary .score-summary .rating b',0)->plaintext;
        $puntaje = $puntaje * 2;
       
        return $puntaje;
    }

    public function capturaOrbitz($url){

        $html = $this->curlUrl($url);

        $puntaje = $html->find('.reviewRatingBubble',0)->plaintext;
        $puntaje = $puntaje * 2;
       
        return $puntaje;
    }

    public function curlUrl($url,$html = true) {
        $ch = curl_init();
        $timeout = 0;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
       // curl_setopt($ch, CURLOPT_HEADER, true); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 

        $header = array();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] =  "Cache-Control: max-age=0";
        $header[] =  "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($ch, CURLOPT_FAILONERROR, 1); // stop when it encounters an error
        curl_setopt($ch, CURLOPT_COOKIESESSION, true );

        curl_setopt($ch, CURLOPT_USERAGENT,  $_SERVER['HTTP_USER_AGENT'] );
        curl_setopt($ch, CURLOPT_AUTOREFERER,true);

        $data = curl_exec($ch);

        curl_close($ch);

        if($html){
            $dom = new \simple_html_dom();
            // Load HTML from a string
            $dom->load($data);
        
            return $dom;
        }else{
            return $data;
        } 
    }



}