<?php

namespace Front\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class MapaPlugin extends AbstractPlugin {

    public function obtenerCoordenadas($hotel) {

    		 $laX = strpos($hotel['mapa'], "@");//obtenemos posicion de la longitud y latitud
        	 $laX = substr($hotel['mapa'], $laX);
        	 $laX2 = strpos($laX, "/");//obtenemos posicion de la longitud y latitud
             $texto = substr($laX,0, $laX2);
             $texto =str_replace(["@","z"],"", $texto);

             $mapa = explode(",", $texto);

    
        return $mapa;

    }

}

?>
