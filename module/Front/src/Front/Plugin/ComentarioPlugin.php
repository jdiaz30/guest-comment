<?php

namespace Front\Plugin; 

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;

class ComentarioPlugin extends AbstractPlugin {


    /*public function capturaComentarios($hotel,$idioma,$booking = NULL, $despegar = NULL, $hotels = NULL, $orbitz = NULL,$expedia = NULL, $paginador, $limit, $translator) {
        //set_time_limit(8090);
        $bookComment = array();
        $orbitzComment = array();
        $despegarComment = array();
        $hotelsComment = array();
        $expediaComment = array();

        if($expedia!=null && $expedia!=""){
            $expediaComment = $this->capturaExpedia($expedia,$idioma);
        }

        if($booking!=null && $booking!=""){
             $bookComment = $this->capturaBooking($booking,$idioma);
        } 

        if($orbitz!=null && $orbitz!=""){
             $orbitzComment = $this->capturaOrbitz($orbitz,$idioma);
        }  

        if($despegar!=null && $despegar!=""){
             $despegarComment = $this->capturaDespegar($despegar,$idioma);
        }
       
        if($hotels!=null && $hotels!=""){
             $hotelsComment = $this->capturaHotels($hotels,$idioma); 
        } 
    
        $commentsMain = array_merge($bookComment,$orbitzComment,$despegarComment,$hotelsComment,$expediaComment);

        $ordenaFecha = create_function(
                '$a, $b', 'return strtotime($b["date"]) - strtotime($a["date"]);'
        );
        
        usort($commentsMain, $ordenaFecha);
        //Guardamos el array con los comentarios en una session para luego paginarlos
        $comentarioSession = new Container("co".$hotel);
        $comentarioSession->comment = $commentsMain;

        unset($commentsMain);

        $comentario = $this->mostrarComentario($comentarioSession,$idioma, $paginador, $limit, $translator);

        return $comentario;
    }*/

    public function capturaComentarios($hotel,$idioma,$booking = NULL, $despegar = NULL, $hotels = NULL, $orbitz = NULL,$expedia = NULL, $paginador, $limit, $translator) {
        set_time_limit(0);

        $bookComment = array();
        $orbitzComment = array();
        $despegarComment = array();
        $hotelsComment = array();
        $expediaComment = array();

        $url = array();

        $url[0] = $booking;
        $url[1] = $despegar;
        $url[2] = $orbitz;
        $url[3] = $hotels;
        $url[4] = $expedia;

        $datosUrl = $this->getUrlNews($url,$idioma);
        //Mandamos las url nuevas a la funcion que scanea las webs
        $curlPlugin = $this->getController()->curlPlugin();

        $dataComment = $curlPlugin->peticionMultiple($datosUrl);
         
        $bookComment = $this->capturaBooking($dataComment,$idioma);
        $despegarComment = $this->capturaDespegar($dataComment);
        $orbitzComment = $this->capturaOrbitz($dataComment,$idioma);
        $hotelsComment = $this->capturaHotels($dataComment,$idioma); 
        $expediaComment = $this->capturaExpedia($dataComment,$idioma);

        $commentsMain = array_merge($bookComment,$orbitzComment,$despegarComment,$hotelsComment,$expediaComment);

        $ordenaFecha = create_function(
                '$a, $b', 'return strtotime($b["date"]) - strtotime($a["date"]);'
        );
        
        usort($commentsMain, $ordenaFecha);
        //Guardamos el array con los comentarios en una session para luego paginarlos
        $comentarioSession = new Container("co".$hotel);
        $comentarioSession->comment = $commentsMain;

        unset($commentsMain);

        $comentario = $this->mostrarComentario($comentarioSession,$idioma, $paginador, $limit, $translator);

        return $comentario;
    }

    public function getDataUrl($data,$web){
          $datos = new \simple_html_dom();

          $datosComment = "";

          switch ($web) {
               case 'Booking':
                    $datosComment = $datos->load($data[0]);
                    break;
                case 'Despegar':
                    $datosComment = $datos->load($data[1]);
                    break;
                case 'Orbitz':
                     $datosComment = $datos->load($data[2]);
                    break;
                case 'Hoteles':
                     $datosComment = $datos->load($data[3]);
                    break;
                case 'Expedia':
                   
                    $datosComment = $data[4];
                    break;
               
               default:
                   # code...
                   break;
           }

           return $datosComment;
    }

    public function getUrlNews($data,$idioma){
        $url = array();

        $url[0] = $this->obtenerUrlNew($data[0],"Booking",$idioma);
     
        $url[1] = $data[1];
        $url[2] = $this->obtenerUrlNew($data[2],"Orbitz",$idioma);

        $urlHotels = $this->obtenerUrlNew("http://www.hoteles.com/hotel/details.html", "hotels",$idioma);
        $id = $this->dataHotels($data[3]);

        $enlace = "?tab=description&hotelId=".$id."&previousDateful=false&display=reviews";
        $urlComentario = $urlHotels . $enlace;

        $url[3] = $urlComentario;
        $url[4] = $this->obtenerUrlNew($data[4],"Expedia",$idioma);

        return $url;
    }

    public function mostrarComentario($comentarioSession,$idioma, $paginador, $limit, $translator) {

        $paginadoComment = array_slice($comentarioSession->comment, $paginador, $limit);

        $nextComment = $paginador + $limit;
        $paginadoNext = array_slice($comentarioSession->comment, $nextComment, $limit);
        $totalNext = count($paginadoNext);

        $iComment = $paginador;
        $comentario = "";

        foreach ($paginadoComment as $d){
            $logo = $this->getLogo($d['logo']);
           //$bandera = $this->getBandera($d['bandera'],$d['logo']);
            $comentario.='<div class="row no comentario-item"><div class="box-comentario">';

            $comentario.='<div class="user-from">
         
            <div class="nombre-user">
            <a href="javascript:void(0)">'.$d['name'].'</a>
            </div>
            <div class="ciudad-user">
            '.$d['from'].'
            </div>

            <div class="col-logo">'.$logo.'</div>
            </div>';

            $comentario.='<div class="comentario-container">
            <div class="box-col-comentario">';

            $puntaje = $this->getPuntaje($d['puntaje']);

            $comentario.='<div class="col-puntaje">
            <div class="puntaje">'.$puntaje.'</div>
            </div>';

            $comentario.='<div class="col-comentario">';

            if(isset($d['date-f'])){
                $fecha = $this->getFecha($d['date'],null,"Hoteles","si");
            }else{
                $fecha = $this->getFecha($d['date']);
            }


            $comentario.='<div class="col-head-comment">';

            if (isset($d['title'])) {
                $comentario.='<div class="titulo-comment">"'.$d['title'].'"</div>';
               
            } else {
                if (isset($d['comment-good']) && isset($d['comment-bad'])) {
                        $comentario.='<div class="titulo-comment">"'. substr($d['comment-good'], 0, 20) .'...'.'"</div>';
                } else {
                    if (isset($d['comment-good'])) {
                        $comentario.='<div class="titulo-comment">"'. substr($d['comment-good'], 0, 20) .'...'.'"</div>';
                    }

                    if (isset($d['comment-bad'])) {
                        $comentario.='<div class="titulo-comment">"'. substr($d['comment-bad'], 0, 20) .'...'.'"</div>';
                    }
                }
            }

            $comentario.='<div class="col-fecha">'.$fecha.'</div>
            </div>';

            $comentario.='<div class="comentario-text">';

            if (isset($d['comment-bad']) && isset($d['comment-good'])) {

                $countComment = strlen($d['comment-good'] . $d['comment-bad']);

                if ($d['logo'] == "Booking") {
                    $comentario.="<p><img src='/img/good.gif' /> " . $d['comment-good'] . "</p>";
                    if ($countComment >= 250) {
                        $comentario.="<p><span id=" . $iComment . " style='display:none'><img src='/img/bad.gif' /> " . $d['comment-bad'] . "</span></p>";
                        $comentario.="<p><a href='javascript:void(0)' onclick='verMas(" . $iComment . ")' id='a" . $iComment . "'>View more</a></p>";
             
                    } else {
                        $comentario.="<p><img src='/img/bad.gif' /> " . $d['comment-bad'] . "</p>";
                    }
                } else {
                    $comentario.="<p>" . $d['comment-good'] . "</p>";

                    if ($countComment >= 250) {
                        $comentario.="<p><span id=" . $iComment . " style='display:none'>" . $d['comment-bad'] . "</span></p>";
                        $comentario.="<p><a href='javascript:void(0)' onclick='verMas(" . $iComment . ")' id='a" . $iComment . "'>View more</a></p>";
             
                    }else{
                        $comentario.="<p>" . $d['comment-bad'] . "</p>";
                    }

                }

            }else{

                 if (isset($d['comment-good'])) {
                     $countComment = strlen($d['comment-good']);

                    if($d['logo']=="Booking"){

                        if ($countComment >= 250) {
                            $comentario.="<p><img src='/img/good.gif' /> " . substr($d['comment-good'], 0, 150);

                            $comentario.="<span id=" . $iComment . " style='display:none'>" . substr($d['comment-good'], 150) . "</span>";
                            $comentario.="</p>";

                            $comentario.="<p><a href='javascript:void(0)' onclick='verMas(" . $iComment . ")' id='a" . $iComment . "'>+</a></p>";
                        }else {
                            $comentario.="<p><img src='/img/good.gif' /> " . $d['comment-good'] . "</p>";
                        }


                    }else{

                        if ($countComment >= 250) {
                            $comentario.="<p>" . substr($d['comment-good'], 0, 150);

                            $comentario.="<span id=" . $iComment . " style='display:none'>" . substr($d['comment-good'], 150) . "</span>";
                            $comentario.="</p>";

                            $comentario.="<p><a href='javascript:void(0)' onclick='verMas(" . $iComment . ")' id='a" . $iComment . "'>View more</a></p>";
                        }else {
                            $comentario.="<p>" . $d['comment-good'] . "</p>";
                        }

                    } 

                 }

                if (isset($d['comment-bad'])) {
                     $countComment = strlen($d['comment-bad']);

                    if($d['logo']=="Booking"){

                        if ($countComment >= 250) {
                            $comentario.="<p><img src='/img/bad.gif' /> " . substr($d['comment-bad'], 0, 150);

                            $comentario.="<span id=" . $iComment . " style='display:none'>" . substr($d['comment-bad'], 150) . "</span>";
                            $comentario.="</p>";

                            $comentario.="<p><a href='javascript:void(0)' onclick='verMas(" . $iComment . ")' id='a" . $iComment . "'>View more</a></p>";
                        }else{
                            $comentario.="<p><img src='/img/bad.gif' /> " . $d['comment-bad'] . "</p>";
                        }

                    }else{
                        if ($countComment >= 250) {
                            $comentario.="<p>" . substr($d['comment-bad'], 0, 150);

                            $comentario.="<span id=" . $iComment . " style='display:none'>" . substr($d['comment-bad'], 150) . "</span>";
                            $comentario.="</p>";

                            $comentario.="<p><a href='javascript:void(0)' onclick='verMas(" . $iComment . ")' id='a" . $iComment . "'>View more</a></p>";
                        }else{
                            $comentario.="<p>" . $d['comment-bad'] . "</p>";
                        }

                    }
                 }

            }
          
            $comentario.='</div>';//comentario-text


            $comentario.='</div>';//col-comentario

            $comentario.='</div>';//box-col-comentario

            $comentario.='</div>';//,comentario-container


            $comentario.='</div></div>';//boxcomentario,comentario item

            $iComment+=1;

        }

        if ($totalNext > 0) {
            $comentario.="<div class='more-comment'><div class='loading-c'><img src='/img/loader-2.gif' width='40'></div><a href='javascript:void(0)' onclick='masComentarios(" . $nextComment . "," . $limit . ")' >" . $translator->translate("Ver más comentarios") . "</a></div>";
        }


        return $comentario;
    }

    public function capturaBooking($data,$idioma) {
     
        $html = $this->getDataUrl($data,"Booking");

        $comment = array();
        $i = 0;
        //$idioma = $this->detectarIdiomaUrl($urlNew, "Booking");
        //echo $idioma;
        if ($html->find('ul[id=simple_comments_list]', 0)) {
            foreach ($html->find('ul[id=simple_comments_list]') as $e):
                foreach ($e->find('li') as $d):
                    if ($d->find('.cell_comments_container .comments_good', 0) || $d->find('.cell_comments_container .comments_bad', 0)) {
                        $comment[$i]['name'] = $d->find('.cell_user .cell_user_name', 0)->plaintext;
                        $comment[$i]['date'] = $this->formatearFecha($d->find('.cell_user .cell_user_date', 0)->plaintext, "Booking", $idioma);
                        $comment[$i]['from'] = $d->find('.user_location .country-name', 0)->plaintext;

                        if ($d->find('.cell_comments_container .comments_good', 0)) {
                            $comment[$i]['comment-good'] = $d->find('.cell_comments_container .comments_good', 0)->plaintext;
                        }

                        if ($d->find('.cell_comments_container .comments_bad', 0)) {
                            $comment[$i]['comment-bad'] = $d->find('.cell_comments_container .comments_bad', 0)->plaintext;
                        }

                        $comment[$i]['logo'] = "Booking";
                        $comment[$i]['puntaje'] = $d->find('.cell_score .the_score', 0)->plaintext;
                        $i+=1;
                    }
                endforeach;
            endforeach;
        } else {
            foreach ($html->find('.review_list') as $e):
                foreach ($e->find('.review_item') as $d):
                    $comment[$i]['name'] = $d->find('.review_item_reviewer h4', 0)->plaintext;
                    //$comment[$i]['date'] = $d->find('.review_item_header_date', 0)->plaintext;
                    
                    $comment[$i]['date'] = $this->formatearFecha($d->find('.review_item_header_date', 0)->plaintext, "Booking", $idioma);
                    $comment[$i]['from'] = $d->find('.reviewer_country', 0)->plaintext;
                    $comment[$i]['puntaje'] = $d->find('.review_item_review_score', 0)->plaintext;
                    $comment[$i]['logo'] = "Booking";
                    $comment[$i]['title'] = trim(str_replace('"', "",$d->find('.review_item_header_content', 0)->plaintext));

                    if ($d->find('.review_pos', 0)) {
                        $comment[$i]['comment-good'] = str_replace('"', "",$d->find('.review_pos', 0)->plaintext);
                    }

                    if ($d->find('.review_smly_pos', 0)) {
                        $comment[$i]['comment-good'] = str_replace('"', "",$d->find('.review_smly_pos', 0)->plaintext);
                    }

                    if ($d->find('.review_neg', 0)) {
                        $commentBad=str_replace('"', "",$d->find('.review_neg', 0)->plaintext);
                        $comment[$i]['comment-bad'] = trim($commentBad);
                    }

                    if ($d->find('.review_smly_neg', 0)) { 
                         $commentBad=str_replace('"', "",$d->find('.review_smly_neg', 0)->plaintext);
                         $comment[$i]['comment-bad'] =trim($commentBad);
                    }

                    $comment[$i]['bandera'] = $d->find('.reviewer_country', 0)->innertext;
                  
                    $i+=1;
                endforeach;
            endforeach;
        }


        return $comment;
    }

    public function capturaDespegar($data) {
        //$html = file_get_html($url);
        $html = $this->getDataUrl($data,"Despegar");

        $comment = array();
        $i = 0;
        foreach ($html->find('div[id=common-comments-list]') as $d):
            foreach ($d->find('.ux-common-comment-bubble') as $e):
                if ($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-best', 0)
                        || $e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-worst', 0)
                        || $e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-description', 0)) {

                    $comment[$i]['name'] = $e->find('.ux-common-comment-bubble-user p', 0)->plaintext;
                    $comment[$i]['date'] = $this->formatearFecha($e->find('.ux-common-comment-bubble-date', 0)->plaintext, "Despegar");
                    $comment[$i]['from'] = $e->find('.ux-common-comment-bubble-user .flagIcon', 0)->title;

                    if ($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-best', 0)) {

                        $comment[$i]['comment-good'] = trim($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-best', 0)->plaintext);
                    }
                    if ($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-worst', 0)) {
                        $comment[$i]['comment-bad'] = trim($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-worst', 0)->plaintext);
                    }

                    if ($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-description', 0)) {
                        $comment[$i]['comment-good'] = trim($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-description', 0)->plaintext);
                    }

                    $comment[$i]['logo'] = "Despegar";
                    $comment[$i]['puntaje'] = str_replace("puntos", "", $e->find('.ux-common-comment-bubble-rate', 0)->plaintext);
                    $comment[$i]['bandera'] = $e->find('.ux-common-comment-bubble-user', 0)->innertext;
                    $i+=1;
                }
            // echo $d->innertext . '<br>';
            endforeach;

        endforeach;

        return $comment;
    }

    public function capturaExpedia($data,$idioma){ 
        $html = $this->getDataUrl($data,"Expedia");

        $comment = array();
        $i = 0;

        $json_comment = json_decode($html,true);

        foreach ($json_comment['reviewDetails']['reviewCollection']['review'] as $data) {  

            if($data['reviewText']!=""){

                if(isset($data['userNickname']) && $data['userNickname'] !="" ){
                    $comment[$i]['name'] = $data['userNickname'];
                }else{
                    $comment[$i]['name'] = "Anónimo";
                }  

                $fecha = substr($data['reviewSubmissionTime'], 0,10);
                $fecha = date("d-m-Y",strtotime($fecha));
            
                $comment[$i]['date'] =  $fecha;
                $comment[$i]['from'] = $data['userLocation'];
                $comment[$i]['puntaje'] = $data['ratingOverall'] * 2;//la puntuacion  tiene que ser sobre 10 y esta en 5
                $comment[$i]['logo'] = "Expedia";
                $comment[$i]['bandera'] = $data['userLocation'];

                if($data['title']==""){
                    $comment[$i]['title'] = substr($data['reviewText'],0,50)." ...";
                }else{
                    $comment[$i]['title'] = str_replace('"', "",$data['title']);
                }

                $comment[$i]['comment-good'] = $data['reviewText'];

                $i+=1;

            }

        }

        return $comment;
    }

    public function capturaOrbitz($data,$idioma) {
      
        $html = $this->getDataUrl($data,"Orbitz");

        $comment = array();
        $i = 0;
        foreach ($html->find('.hotelRecentReviews') as $e):
            foreach ($e->find('.userReview') as $d):
                $comment[$i]['name'] = $d->find('.reviewDetails .reviewerName .reviewer', 0)->plaintext;
                $comment[$i]['date'] = date("d-m-Y", strtotime($d->find('.dtreviewed', 0)->title));
                $comment[$i]['from'] = $d->find('.location', 0)->plaintext;
                $comment[$i]['title'] = str_replace('"', "",$d->find('.summary', 0)->plaintext);
                $comment[$i]['comment-good'] = trim($d->find(' .description', 0)->plaintext);
                $comment[$i]['logo'] = "Orbitz";
                $comment[$i]['puntaje'] = $d->find('.rating', 0)->plaintext * 2;
                $comment[$i]['bandera'] = "no hay";
                
                $i+=1;
            endforeach;
        endforeach;

        return $comment;
    }

    public function capturaHotels($data,$idioma) {
        $htmlComentario = $this->getDataUrl($data,"Hoteles");

        $comment = array();
        $i = 0;

        foreach ($htmlComentario->find(".reviews") as $c):
            foreach ($c->find(".review")as $data):
  
                if($data->find(".reviewer .fn")){
                    $comment[$i]['name'] = $data->find(".reviewer .fn", 0)->plaintext;

                }else{
                    
                    $co = substr(trim($data->find(".reviewer", 0)->plaintext),0,-2);
                    $comment[$i]['name'] = $co;

                }

                if ($data->find(".value-title")) {
                    $comment[$i]['date'] = date("d-m-Y", strtotime($data->find(".value-title", 0)->title));
                } else {
                    $comment[$i]['date'] = $this->formatearFecha($data->find(".dtreviewed", 0)->plaintext, "hotels", $idioma);
                    $comment[$i]['date-f'] ="si";
                }

                $comment[$i]['from'] = $data->find(".locality", 0)->plaintext;
                $comment[$i]['bandera'] = $data->find(".locality", 0)->plaintext;

                $noVa = [
                    "Más información",
                    "Comentario auténtico de un cliente de Hoteles.com",
                    "Comentario auténtico de un huésped de Expedia",
                    "Comentario auténtico de un cliente de Hotels.com",
                    "Read more Genuine Hotels.com guest review",
                    "Genuine Hotels.com guest review",
                    "Genuine Expedia guest review",
                    "Read more",
                    "Comentario real de un huésped de Hoteles.com"];
                $comment[$i]['comment-good'] = trim(str_replace($noVa, "", $data->find(".description", 0)->plaintext));
                $comment[$i]['logo'] = "Hoteles";
               // $comment[$i]['title'] =  trim(str_replace('"', "",$data->find(".badge", 0)->plaintext . $data->find(".summary", 0)->plaintext));
                 $comment[$i]['title'] = trim(str_replace('"', "",$data->find(".summary", 0)->plaintext));
                
                $comment[$i]['puntaje'] = $data->find(".rating strong", 0)->plaintext * 2;

                $i+=1;

            endforeach;
        endforeach;

        return $comment;
    }

    public function formatearFecha($fecha, $urlWeb, $idioma = NULL) {
        $newFecha = "";
        $anio = "";
        $mes = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "septiembre", "Octubre", "Noviembre", "Diciembre");
        $mesIng = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $xDia = "";
        $xMes = "";
        //Capturamos el dia
        for ($i = 1; $i <= 31; $i++) {
            $pos = strpos($fecha, $i);
            $xDia = substr($fecha, $pos, 3);

            if ($xDia == $i) {
                break;
            }
        }

        $anio = trim(substr($fecha, -5));
        switch ($urlWeb) {
            case 'Booking':
                //Capturamos el mes
                if ($idioma == "en_US") {
                    for ($j = 0; $j <= 11; $j++) {
                        $minusMes = $mesIng[$j];

                        $posX = strpos($fecha, $minusMes);
                        $xMes = substr($fecha, $posX, strlen($minusMes));

                        if ($xMes == $minusMes) {
                            $xMes = $j + 1;
                            break;
                        }
                    }

                    $posFecha = strpos($fecha, ",");

                    $xDia = substr($fecha, $posFecha-2,2);
                
                   // $xDia = str_replace(["2014", "2013", "2012", "2011", ","], "", $dia);
                } else {
                    for ($j = 0; $j <= 11; $j++) {
                        $minusMes = strtolower($mes[$j]);

                        $posX = strpos($fecha, $minusMes);
                        $xMes = substr($fecha, $posX, strlen($minusMes));

                        if ($xMes == $minusMes) {
                            $xMes = $j + 1;
                            break;
                        }
                    }
                    //Dia
                    $xDia = str_replace("d", "", $xDia);
                }

                $newFecha = trim($xDia) . "-" . $xMes . "-" . $anio;
                // $newFecha=$xMes." ".$xDia.", ".$anio;

                break;
            case 'Despegar':

                //Capturamos el mes
                for ($j = 0; $j <= 11; $j++) {
                    $minusMes = strtolower(substr($mes[$j], 0, 3));
                    $posX = strpos($fecha, $minusMes);
                    $xMes = substr($fecha, $posX, strlen($minusMes));

                    if ($xMes == $minusMes) {
                        $xMes = $j + 1;
                        break;
                    }
                }
                $newFecha = trim($xDia) . "-" . $xMes . "-" . $anio;
                break;

            case 'hotels':
                //Capturamos el mes
                if ($idioma == "en_US") {
                    for ($j = 0; $j <= 11; $j++) {
                        //$minusMes=strtolower(substr($mes[$j],0,3));
                        $minusMes = substr($mesIng[$j], 0, 3);
                        $posX = strpos($fecha, $minusMes);
                        $xMes = substr($fecha, $posX, strlen($minusMes));

                        if ($xMes == $minusMes) {
                            $xMes = $j + 1;
                            break;
                        }
                    }
                } else {
                    for ($j = 0; $j <= 11; $j++) {
                        $minusMes = strtolower(substr($mes[$j], 0, 3));
                        $posX = strpos($fecha, $minusMes);

                        $xMes = substr($fecha, $posX, strlen($minusMes));
                        if ($xMes == $minusMes) {
                            $xMes = $j + 1;
                            break;
                        }
                    }
                }

                $xDia = 1;
                $newFecha = trim($xDia) . "-" . $xMes . "-" . $anio;

                break;
        }

        return $newFecha;
    }

    public function ordenar($a, $b) {
        return strtotime($a['date']) - strtotime($b['date']);
    }

    public function validarFecha($fecha) {
        if (ereg("(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}", $fecha)) {
            return true;
        } else {
            return false;
        }
    }

    public function obtenerUrlNew($url,$web,$idioma) {
        $languageSession = new Container("language");
        $urlNew = "";

        switch ($web) {
            case 'Booking':
                $dataBook = $this->dataBooking($url);
                $idiomaUrl = $this->capturaIdiomaUrl($idioma, "Booking");
                //$urlNew = "http://www.booking.com/hotel/" . $cadenaFormat . $idiomaUrl . "html";
                $urlNew = $idiomaUrl."pagename=".$dataBook['name'].";cc1=".$dataBook['pais'].";type=total;dist=1;offset=0;rows=5;rid=;sort=f_recent_desc";

                break;
            case 'Orbitz':

                $idiomaUrl = $this->capturaIdiomaUrl($idioma, "Orbitz");
                $cadena = $this->dataOrbitz($url);

                $urlNew =  $idiomaUrl . $cadena;

                break;

            case 'hotels':
                $pos = strpos($url, "hoteles.com");
                if ($pos != true) {
                    $pos = strpos($url, "hotels");
                }

                $cadena = substr($url, $pos);
                $cadenaFormat = str_replace(["hoteles.com", "hotels.com"], "", $cadena);

                $idiomaUrl = $this->capturaIdiomaUrl($idioma, "hotels");

                $urlNew = $idiomaUrl . $cadenaFormat;

                break;

            case 'Expedia':
                $id = $this->dataExpedia($url);

                $idiomaUrl = $this->capturaIdiomaUrl($idioma, "Expedia");

                $urlNew = "http://reviewsvc.expedia.com/reviews/v1/retrieve/getReviewsForHotelId/".$id."/?_type=json&start=0&items=10&sortBy=DATEDESC&categoryFilter=&languageFilter=&languageSort=".$idiomaUrl."&pageName=page.Hotels.Infosite.Information";

                break;

            default:
                # code...
                break;
        }
    
        return $urlNew;
    }

    public function getLogo($logo){
        $imgLogo = "";
        switch ($logo) {
            case 'Booking':
                $imgLogo ="<img src='/img/otas/booking.png'  width='100' />";
                break;

            case 'Despegar':
                $imgLogo ="<img src='/img/otas/despegar.png'  width='100' />";
                break;

            case 'Expedia':
                $imgLogo ="<img src='/img/otas/expedia.png'  width='100' />";
                break;
            
            case 'Orbitz':
                $imgLogo ="<img src='/img/otas/orbitz.png'  width='100' />";
                break;

            case 'Hoteles':
                $imgLogo ="<img src='/img/otas/hoteles.png'  width='100' />";
                break;
        }

        return $imgLogo;
    }

    public function getBandera($pais,$web){
       
        $bandera = "";

        switch ($web) {
            case 'Booking':
                $pos = strpos($pais ,"slang-");
                $xPais = substr($pais, $pos);
                $xPais = str_replace(["slang-",'"',">"],"", $xPais);

                $bandera = '<span class="flag-icon flag-icon-'.$xPais.'"></span>';
                break;

            case 'Despegar':
                $pos = strpos($pais ,"flag-");
                $xPais = substr($pais, $pos);
                $xPais = str_replace(["flag-"],"", $xPais);
                $xPais = substr($xPais, 0,2);

                $bandera = '<span class="flag-icon flag-icon-'.$xPais.'"></span>';

                break;

            case 'Orbitz':
                $bandera = '';
                break;

            case 'Hoteles':
                $xPais = strtolower($pais);
                $bandera = '<span class="flag-icon flag-icon-'.$xPais.'"></span>';
                break;
            
            default:
                $bandera = '';
                break;
        }

        return $bandera;
    }

    public function dataBooking($url){
        $formateaUrl = str_replace("http://www.booking.com/hotel/", "", $url);
                 
        $pos = strpos($formateaUrl, ".");
        $cadena = substr($formateaUrl, $pos);
        $cadenaFormat = str_replace($cadena, "", $formateaUrl);

        $data['name'] = substr($cadenaFormat,3);
        $data['pais'] = substr($cadenaFormat, 0,2);

        return $data;
    }

    public function dataExpedia($url){
        $pos = strpos($url ,".h");
        $id = substr($url, $pos,9);
        $id = str_replace(".h","", $id);

        return $id;
    }

    public function dataHotels($url){

        $pos = strpos($url ,"hotelId");

        if($pos==true){
          $id = substr($url, $pos,14);
          $id = str_replace("hotelId=","", $id);
        }else{
          $pos = strpos($url ,"/ho");
          $id = substr($url, $pos,9);
          $id = str_replace("/ho","", $id);
        }
        
        return trim($id);
    }

    public function dataOrbitz($url){

        $pos = strpos($url ,".h");
        $id = substr($url, $pos);
        $id = str_replace(["h",".","/"],"", $id);

        return $id;
    }

    public function capturaIdiomaUrl($idioma, $web) {
        $prefijoUrl = "";
        switch ($web) {
            case 'Booking':
                switch ($idioma) {
                    case 'en_US':
                        $prefijoUrl = "http://www.booking.com/reviewlist.en.html?";
                        break;
                    case 'es_ES':
                        $prefijoUrl = "http://www.booking.com/reviewlist.es.html?";
                        break;
                }

                break;
            case 'Orbitz':
                switch ($idioma) {
                    case 'en_US':
                        $prefijoUrl = "http://www.orbitz.com/shop/hotelsearch?prefLang=en&hsv.showDetails=true&type=hotel&hotel.type=keyword&search=Search&hotel.hid=";
                        break;
                    case 'es_ES':
                        $prefijoUrl = "http://www.orbitz.com/shop/hotelsearch?prefLang=es&hsv.showDetails=true&type=hotel&hotel.type=keyword&search=Search&hotel.hid=";
                        break;

            
                }

                break;

            case 'hotels':
                switch ($idioma) {
                    case 'en_US':
                        $prefijoUrl = "http://www.hotels.com";
                        break;
                    case 'es_ES':
                        $prefijoUrl = "http://es.hoteles.com";
                        break;
                }
                break;

            case 'Expedia':

            switch ($idioma) {
                case 'en_US':
                    $prefijoUrl = "en";
                    break;

                 case 'es_ES':
                    $prefijoUrl = "es";
                    break;
                
                default:
                    $prefijoUrl = "es";
                    break;
            }
            break;

            default:
                # code...
                break;
        }

        return $prefijoUrl;
    }

    public function detectarIdiomaUrl($url, $web) {
        $idioma = "";
        switch ($web) {
            case 'Booking':
                /*$formateaUrl = str_replace("http://www.booking.com/hotel/", "", $url);
                $pos = strpos($formateaUrl, ".");
                $cadena = substr($formateaUrl, $pos);
                $cadenaFormat = str_replace(["html", "."], "", $cadena);*/

                $pos = strpos($url,".en.");

                if($pos){
                    $idioma = "en_US";
                }else{
                    $idioma = "es_ES";

                }

                break;

            case 'hotels':
                $pos = strpos($url, "es.hotels.com");
                if ($pos == true) {
                    $idioma = "es_ES";
                } else {
                    $idioma = "en_US";
                }
                break;

            default:
                # code...
                break;
        }

        return $idioma;
    }

    public function calcularPromedio($idHotel) {
        $comentarioSession = new Container("co".$idHotel);

        $puntaje = 0;
        $promedio = 0;

        if(count($comentarioSession->comment)>0){

            foreach ($comentarioSession->comment as $data) {
                $puntaje+=$data['puntaje'];
            }

            $promedio = $puntaje / count($comentarioSession->comment);

            $promedio = number_format($promedio, 1, ',', ' '); 

        }

        return $promedio;
    }

    public function getPuntaje($data){

        $puntaje = number_format((float)$data, 1, ',', ' ');
        
        if($puntaje == "10,0"){
           $puntaje = "10";
        }

        return $puntaje;
      
    }

    public function getFecha($fecha,$idioma=null,$web=null,$noDia=null){
    
        $mesEs = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "septiembre", "Octubre", "Noviembre", "Diciembre");
        $mesIng = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
       
        $separa = explode("-",$fecha);
        $mes = $separa[1];
        $dia = $separa[0];
        $anio = $separa[2];

        $newFecha = "";
        if(isset($idioma)){

           switch ($idioma) {
               case 'en_US':
                   $newFecha = $dia." ".$mesIng[$mes-1]." , ".$anio;
                   break;
               case 'es_ES':
                   $newFecha = $dia." de ".$mesEs[$mes-1]." del ".$anio;
                   break;

           }

        }else{

            if($web=="Hoteles" && $noDia=="si"){
                $newFecha = $mesEs[$mes-1]." del ".$anio;
            }else{
                $newFecha = $dia." de ".$mesEs[$mes-1]." del ".$anio;

            }

        }

        return $newFecha;
    }

}

?>
