<?php

namespace Front\Plugin; 

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;

class MotorReservasPlugin extends AbstractPlugin {
  
    public function verDisponibilidad($data){

        $htmlResponse = "";
        $curlPlugin = $this->getController()->curlPlugin();

        $booking = $this->armaUrl($data,"Booking");
        $hoteles = $this->armaUrl($data,"Hoteles");
        $expedia = $this->armaUrl($data,"Expedia");
        $orbitz= $this->armaUrl($data,"Orbitz");

        $url = array();

        $url[0] = $booking;
        $url[1] = $hoteles;
        $url[2] = $expedia;
        $url[3] = $orbitz;

        $datosMotor = $curlPlugin->peticionMultiple($url);

        $htmlResponse .= $this->capturaBooking($datosMotor[0],$booking,$data['fecha-ini'],$data['fecha-sal']);
            
        $htmlResponse .= $this->capturaHoteles($datosMotor[1],$hoteles,$data['fecha-ini'],$data['fecha-sal']);
  
        $htmlResponse .= $this->capturaExpedia($datosMotor[2],$expedia,$data['fecha-ini'],$data['fecha-sal']);
          
        $htmlResponse .= $this->capturaOrbitz($datosMotor[3],$orbitz,$data['fecha-ini'],$data['fecha-sal']);

        if($htmlResponse == ""){
            $htmlResponse = "<div class='center no-items'>No hay disponibilidad</div>";
        }
       

        return $htmlResponse;
    }

    public function  armaUrl($data,$web){

        $fechaIni = explode('-',$data['fecha-ini']);
        $fechaSal = explode('-',$data['fecha-sal']);

        $newUrl = "";

        switch ($web) {
            case 'Booking':
               
                $fechaIniDay = $fechaIni[2];
                $fechaIniYearMonth =$fechaIni[0]."-".$fechaIni[1];

                $fechaSalDay = $fechaSal[2];
                $fechaSalYearMonth =$fechaSal[0]."-".$fechaSal[1];

                $urlDispone = "checkin_monthday=".$fechaIniDay.";checkin_year_month=".$fechaIniYearMonth.";checkout_monthday=".$fechaSalDay.";checkout_year_month=".$fechaSalYearMonth.";no_rooms=1;req_adults=2;req_children=0&selected_currency=USD";

                $posUrl = strpos("?",$data['booking']);
                if ($posUrl) {
                   $url = substr($data['booking'],0,$posUrl);
                }else{
                    $url = $data['booking']."?";
                }         
                
                $newUrl = $url.$urlDispone;

                break;

            case 'Hoteles':
                $fechaIniX = $fechaIni[2]."-".$fechaIni[1]."-".$fechaIni[0];
                $fechaSalX = $fechaSal[2]."-".$fechaSal[1]."-".$fechaSal[0];

                $id = $this->dataHotels($data['hoteles']);
               
                $urlDefault = "http://es.hotels.com/hotel/details.html?";

                //$urlDispone = "hotelId=".$id."&searchViewType=LIST&validate=true&previousDateful=false&searchViewType=LIST&validate=true&previousDateful=false&arrivalDate=".$fechaIniX."&departureDate=".$fechaSalX."&locale=es_PE&pos=HCOM_LATAM";
                $urlDispone = "hotelId=".$id."&searchViewType=LIST&validate=true&previousDateful=false&searchViewType=LIST&validate=true&previousDateful=false&arrivalDate=".$fechaIniX."&departureDate=".$fechaSalX."&cur=USD";

                $newUrl = $urlDefault.$urlDispone;

                break;

            case 'Expedia':

                $fechaIniX = $fechaIni[1]."/".$fechaIni[2]."/".$fechaIni[0];
                $fechaSalX = $fechaSal[1]."/".$fechaSal[2]."/".$fechaSal[0];

                $urlDefault = $data['expedia'];

                $urlDispone = "?chkin=".$fechaIniX."&chkout=".$fechaSalX."&rm1=a2";

                $newUrl = $urlDefault.$urlDispone;
            
                break;

            case 'Despegar':

                $dataHotel = $this->dataDespegar($data['url']);

                $urlDispone = "/search/Hotel/Details/".$dataHotel['id-hotel']."/".$data['fecha-ini']."/".$data['fecha-sal']."/2";

                $newUrl = $dataHotel['url'].$urlDispone;

                break;

            case 'Orbitz':

                $fechaIniX = $fechaIni[1]."/".$fechaIni[2]."/".$fechaIni[0];
                $fechaSalX = $fechaSal[1]."/".$fechaSal[2]."/".$fechaSal[0];

                $urlDispone = "?type=hotel&hotel.chkin=".$fechaIniX."&hotel.chkout=".$fechaSalX."&hotel.rooms%5B0%5D.adlts=2&hotel.rooms%5B0%5D.chlds=0&hotel.rooms%5B0%5D.chldAge%5B0%5D=&hotel.rooms%5B0%5D.chldAge%5B1%5D=&hotel.rooms%5B0%5D.chldAge%5B2%5D=&hotel.rooms%5B0%5D.chldAge%5B3%5D=&hotel.rooms%5B0%5D.chldAge%5B4%5D=&search=Find";
                
                $newUrl = $data['orbitz'].$urlDispone;

                break;
            
            default:
                # code...
                break;
        }

        return $newUrl;
    }

    public function capturaBooking($data,$urlNew,$fechaIni,$fechaSal){
      
        $html = $this->getData($data);

        $htmlResponse = "";

        if($html->find('#room_availability_container',0)){

            $totalDays = $this->dateDiff($fechaIni,$fechaSal);

            $precio = $html->find('.rooms-table-room-price',0)->plaintext;

            $precio = str_replace(array("US","$"), "", $precio);
            $precio = ceil($precio / $totalDays);

            $htmlResponse .='<div class="row no ota">';
            $htmlResponse .='<div class="logo-ota">
            <a href='.$urlNew.' target="_blank">
              <img src="/img/otas/booking.png" width="90">
            </a>
            </div>';

            $htmlResponse .='<div class="precios-link">
              <div class="precio">
               <a href='.$urlNew.' target="_blank"> $'.$precio.'</a>
              </div>
              <a class="precio-caret" href='.$urlNew.' target="_blank">
                   <i class="x-caret"></i>
              </a>
            </div>';

            $htmlResponse .='</div>';


        }

        return $htmlResponse;
    }

    public function capturaHoteles($data,$urlNew,$fechaIni,$fechaSal){

        $htmlResponse = "";
        $html = $this->getData($data);

        if($html->find('#book_now_nudge',0)){

            $totalDays = $this->dateDiff($fechaIni,$fechaSal);

            $precio = $html->find('.current-price',0)->plaintext;

           // $precio = preg_replace('/[^0-9]/','',$precio);

            //$precio = ($precio / $totalDays);

           // $precio = "S/.".number_format($precio, 2, '.', ' '); 

            $htmlResponse .='<div class="row no ota">';
            $htmlResponse .='<div class="logo-ota">
            <a href='.$urlNew.' target="_blank">
                 <img src="/img/otas/hoteles.png" width="90">
            <a/>
            </div>';

            $htmlResponse .='<div class="precios-link">
              <div class="precio">
              <a href='.$urlNew.' target="_blank">'.$precio.'
              </a>
              </div>
              <a class="precio-caret" href='.$urlNew.' target="_blank">
                   <i class="x-caret"></i>
              </a>
            </div>';

            $htmlResponse .='</div>';
        }

        return $htmlResponse;
    }

    public function capturaExpedia($data,$urlNew,$fechaIni,$fechaSal){ 
        $htmlResponse = "";
        $html = $this->getData($data);

        if($html->find('#room-type-header',0)){

            $totalDays = $this->dateDiff($fechaIni,$fechaSal);

            $precio = $html->find('#lead-price .link-to-rooms',0)->plaintext;

           // $precio = preg_replace('/[^0-9]/','',$precio);

            //$precio = ($precio / $totalDays);

            //$precio = number_format($precio, 2, '.', ' '); 

            $htmlResponse .='<div class="row no ota">';
            $htmlResponse .='<div class="logo-ota">
            <a href='.$urlNew.' target="_blank"><img src="/img/otas/expedia.png" width="90"></a>
            </div>';

            $htmlResponse .='<div class="precios-link">
              <div class="precio">
                 <a href='.$urlNew.' target="_blank">'.$precio.'
                 </a>
              </div>
              <a class="precio-caret" href='.$urlNew.' target="_blank">
                   <i class="x-caret"></i>
              </a>
            </div>';

            $htmlResponse .='</div>';

        }

        return $htmlResponse;
    }

    public function capturaDespegar($urlNew,$fechaIni,$fechaSal){
        $htmlResponse = "";
        $html = $this->curlUrl($urlNew);

        if($html->find('.detailFlap #available-rooms',0)){

            $totalDays = $this->dateDiff($fechaIni,$fechaSal);

            $precio = $html->find('.ux-common-price',0)->plaintext;

            $htmlResponse .= '<div class="precio">'.$precio.'</div>';
            $htmlResponse .= '<div class="btn-oferta"><a href='.$urlNew.' target="_blank">Ver oferta</a>
            </div>';

        }else{
            $htmlResponse = "No hay disponibilidad";
        }

        return $htmlResponse;
    }

    public function capturaOrbitz($data,$urlNew,$fechaIni,$fechaSal){
        $htmlResponse = "";
        $html = $this->getData($data);

        if($html->find('.priceAndDisclaimers',0)){

            $totalDays = $this->dateDiff($fechaIni,$fechaSal);

            $precio = $html->find('.rate',0)->plaintext;

            if($html->find('.secondaryPrice',0)){
                $precio = $html->find('.secondaryPrice .rate',0)->plaintext;
            }

            $htmlResponse .='<div class="row no ota">';
            $htmlResponse .='<div class="logo-ota">
            <a href='.$urlNew.' target="_blank"><img src="/img/otas/orbitz.png" width="90">
            </a>
            </div>';

            $htmlResponse .='<div class="precios-link">
              <div class="precio">
              <a href='.$urlNew.' target="_blank">'.$precio.'
              </a>
              </div>
              <a class="precio-caret" href='.$urlNew.' target="_blank">
                   <i class="x-caret"></i>
              </a>
            </div>';

            $htmlResponse .='</div>';

        }

        return $htmlResponse;  
    }

    public function curlUrl($url,$html = true) {
        $ch = curl_init();
        $timeout = 100;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $header = array();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] =  "Cache-Control: max-age=0";
        $header[] =  "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        curl_setopt($ch, CURLOPT_USERAGENT,  $_SERVER['HTTP_USER_AGENT'] );
        curl_setopt($ch, CURLOPT_AUTOREFERER,1);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);

        $data = curl_exec($ch);

        curl_close($ch);

        if($html){
            $dom = new \simple_html_dom();
            // Load HTML from a string
            $dom->load($data);
        
            return $dom;
        }else{
            return $data;
        } 
    }

    public function getData($data,$html = true) {
       
        if($html){
            $dom = new \simple_html_dom();
            // Load HTML from a string
            $dom->load($data);
        
            return $dom;
        }else{
            return $data;
        } 
    }
     
    public function dataBooking($url){
        $formateaUrl = str_replace("http://www.booking.com/hotel/", "", $url);
                 
        $pos = strpos($formateaUrl, ".");
        $cadena = substr($formateaUrl, $pos);
        $cadenaFormat = str_replace($cadena, "", $formateaUrl);

        $data['name'] = substr($cadenaFormat,3);
        $data['pais'] = substr($cadenaFormat, 0,2);

        return $data;
    }

    public function dataExpedia($url){
        $pos = strpos($url ,".h");
        $id = substr($url, $pos,9);
        $id = str_replace(".h","", $id);

        return $id;
    }

    public function dataHotels($url){

        $pos = strpos($url ,"hotelId");

        if($pos==true){
          $id = substr($url, $pos,14);
          $id = str_replace("hotelId=","", $id);
        }else{
          $pos = strpos($url ,"/ho");
          $id = substr($url, $pos,9);
          $id = str_replace("/ho","", $id);
        }
        
        return trim($id);
    }

    public function dataOrbitz($url){

        $pos = strpos($url ,".h");
        $id = substr($url, $pos);
        $id = str_replace(["h",".","/"],"", $id);

        return $id;
    }
    
    public function dataDespegar($url){

        $pos = strpos($url ,"/hoteles/");

        $idPos = strpos($url,"/h-");
        $idPos = substr($url, $idPos,9);
        $idPos = str_replace("/h-","", $idPos);

        $data['url'] = substr($url, 0,$pos);
        $data['id-hotel'] = $idPos;

        return $data;
    }

    public function dateDiff($start, $end) {

        $start_ts = strtotime($start);

        $end_ts = strtotime($end);

        $diff = $end_ts - $start_ts;

        return round($diff / 86400);
    }

}

?>
