<?php

namespace Front\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class CurlPlugin extends AbstractPlugin {

  public function peticionMultiple($urls){
        $mh = curl_multi_init();
        $i = 0;

        foreach ($urls as $url) {
            $conn[$i]=curl_init($url);

            curl_setopt($conn[$i],CURLOPT_RETURNTRANSFER,1);//return data as string 
            curl_setopt($conn[$i],CURLOPT_FOLLOWLOCATION,1);//follow redirects
           // curl_setopt($conn[$i],CURLOPT_MAXREDIRS,2);//maximum redirects
            //curl_setopt($conn[$i],CURLOPT_CONNECTTIMEOUT,10);//timeout
              // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); 

            $header = array();
            $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
            $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
            $header[] =  "Cache-Control: max-age=0";
            $header[] =  "Connection: keep-alive";
            $header[] = "Keep-Alive: 300";
            //$header[] = "Connection: close";
            //$header[] = "Content-Length: 0";
            $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
            $header[] = "Accept-Language: en-us,en;q=0.5";
            $header[] = "Pragma: "; 

            curl_setopt($conn[$i], CURLOPT_HTTPHEADER, $header);
            curl_setopt($conn[$i], CURLOPT_USERAGENT,  $_SERVER['HTTP_USER_AGENT'] );
            curl_setopt($conn[$i], CURLOPT_AUTOREFERER,1);
            curl_setopt($conn[$i], CURLOPT_NOSIGNAL, 1);
            curl_setopt($conn[$i], CURLOPT_FORBID_REUSE, 1);
            curl_setopt($conn[$i], CURLOPT_FRESH_CONNECT, 1);
           // curl_setopt($conn[$i], CURLOPT_DNS_CACHE_TIMEOUT, 1);
            //curl_setopt($conn[$i], CURLOPT_DNS_USE_GLOBAL_CACHE, false);//mejor no
            
            curl_multi_add_handle ($mh,$conn[$i]);

            $i+=1;
        }

        do { $n=curl_multi_exec($mh,$active); } while ($active);

        $j = 0;

        foreach ($urls as $url) {
            $res[$j]=curl_multi_getcontent($conn[$j]);
            curl_multi_remove_handle($mh,$conn[$j]);
            curl_close($conn[$j]);

            $j+=1;
        }
        curl_multi_close($mh);

        return $res;
  }

  public function curlUrl($url,$html = true) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
       // curl_setopt($ch, CURLOPT_HEADER, true); 
       // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 


        $header = array();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] =  "Cache-Control: max-age=0";
        $header[] =  "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($ch, CURLOPT_FAILONERROR, 1); // stop when it encounters an error
        //curl_setopt($ch, CURLOPT_COOKIESESSION, true );

        curl_setopt($ch, CURLOPT_USERAGENT,  $_SERVER['HTTP_USER_AGENT'] );
        curl_setopt($ch, CURLOPT_AUTOREFERER,true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        
        $data = curl_exec($ch);

        $info = curl_getinfo($ch);

        //echo $url."----".$info['total_time']." ---- ".$info['namelookup_time']."</br>";

        curl_close($ch);

        if($html){
            $dom = new \simple_html_dom();
            // Load HTML from a string
            $dom->load($data);
        
            return $dom;
        }else{
            return $data;
        } 
  }

  /*public  function peticionMultiple($urls, $opciones = array()) {
        
        $resultado = array();

        $mc = \EpiCurl::getInstance();

       // $resultado[0] = $mc->addURL($urls[0])->data; //Booking
        $resultado[1] = $mc->addURL($urls[1])->data; //Despegar
       $resultado[2] = $mc->addURL($urls[2])->data; //Orbitz
        $resultado[3] = $mc->addURL($urls[3])->data; //Hoteles
        $resultado[4] = $mc->addURL($urls[4])->data; //Expedia

        return $resultado;
        
    }*/


}

?>
