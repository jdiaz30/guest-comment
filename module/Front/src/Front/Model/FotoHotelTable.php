<?php

namespace Front\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class FotoHotelTable extends AbstractTableGateway {

	protected $table = 'tb_foto_hotel';

	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}

	public function add($datos = array()){
		$this->insert($datos);
	}

	public function deleteFoto($id) {
        $this->delete(array("id_hotel" => $id));
    }

	public function getAllHotel($hotel){
		$sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from(array("hf" => $this->table));
        $select->join(array("f" =>"tb_foto"),"hf.id_foto = f.id_foto");
        $select->join(array("h" =>"tb_hotel"),"hf.id_hotel = h.id_hotel");
        $select->where(array("hf.id_hotel" => $hotel)); //Habilitados

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
	}

}

?>