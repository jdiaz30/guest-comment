<?php

namespace Front\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate;

class HotelTable extends AbstractTableGateway {

	protected $table = 'tb_hotel';

	public function __construct(Adapter $adapter) {
			$this->adapter = $adapter;
	}

	public function getMaxId(){
		$expresion = new Expression("MAX(id_hotel)");
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from($this->table);

		$select->columns(array("total" => $expresion));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$row = $result->current(); //Mostramos solo un registro

		return $row;
	}

	public function getAllId($id){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from($this->table);

		$select->where(array("id_hotel" => $id));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$row = $result->current(); //Mostramos solo un registro

		return $row;
	}

	public function getAllUrl($url){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("h"=>$this->table));
		$select->columns(array(
			"booking_url",
			"orbitz_url",
			"despegar_url",
			"hoteles_url",
			"expedia_url",
			"nombre",
			"direccion",
			"calificacion",
			"puntaje",
			"mapa",
			"id_hotel",
			"localidad"));

		$select->join(array("c"=>"tb_ciudad"),"c.id_ciudad = h.id_ciudad",array("ciudad"=>"Name","id_ciudad"));
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais",array("pais"=>"Name","Continent","id_pais"));

		$select->where(array("h.url" => $url));
		$select->where->isNotNull("h.puntaje");

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$row = $result->current(); //Mostramos solo un registro

		return $row;
	}

	public function add($datos = array()){
		 $this->insert($datos);
	}

	public function updateHotel($datos = array()){
		 $this->update($datos, array("id_hotel" => $datos['id_hotel']));
	}

	public function getAll(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("a" => $this->table));
		$select->order(array("id_hotel asc"));

	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getAllActive(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("a" => $this->table));
		$select->where(array("a.estado"=>"0"));
		$select->order(array("id_hotel asc"));

	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	/*public function getHotelFiltro($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('a' => $this->table));

		$select->columns(array('value' => new Expression(
        		"CONCAT(?, ' , ', ?)", array('nombre', 'localidad')
   		)));

		$select->where->like("nombre",$data.'%');
		$select->where(array("a.estado"=>"0"));
		//$select->where(array("nombre"=>$data));

		$select2 = $sql->select();
		$select2->from(array("c" =>"tb_ciudad"));
		$select2->columns(array("value"=>"Name"));
		$select2->where->like("Name",$data.'%');

		//$select->order(array("nombre asc"));
		$select->combine($select2);
	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}*/


	public function getLocalidadFiltro($data){
		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('a' => $this->table));
		$select->columns(array("localidad"));

		$select->join(array('c'=> 'tb_ciudad'),"a.id_ciudad = c.id_ciudad",array('ciudad'=>'Name'));
		$select->join(array('p'=> 'tb_pais'),"c.id_pais = p.id_pais",array('pais'=>'Name'));

		$select->where->like("a.localidad",$data.'%');

		$select->quantifier('DISTINCT');

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getCiudadFiltro($data){
		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('c'=> 'tb_ciudad'));
		$select->columns(array('ciudad'=>'Name'));

		$select->join(array('p'=> 'tb_pais'),"c.id_pais = p.id_pais",array('pais'=>'Name'));

		$select->where->like("c.Name",$data.'%');

		$select->quantifier('DISTINCT');

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getPaisFiltro($data){
		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('p'=> 'tb_pais'));
		$select->columns(array('value'=>'Name'));
		
		$select->where->like("p.Name",$data.'%');


		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getHotelFiltro($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('a' => $this->table));
		$select->join(array('c'=> 'tb_ciudad'),"a.id_ciudad = c.id_ciudad",array('ciudad'=>'Name'));
		$select->join(array('p'=> 'tb_pais'),"c.id_pais = p.id_pais",array('pais'=>'Name'));

		$select->where->like("nombre",'%'.$data.'%');
		$select->where(array("a.estado"=>"0"));
		//$select->where(array("nombre"=>$data));

	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function searchCountry($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array("h"=>$this->table));
		$select->join(array("c"=>"tb_ciudad"),"h.id_ciudad = c.id_ciudad");
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais",array("pais"=>"Name"));

		$select->where
       		->nest
       		    ->and->equalTo('p.id_pais', $data)
      			->and->equalTo('h.estado', 0)
       			//->and->notEqualTo("h.despegar_url","")
       			->and->notEqualTo("h.booking_url","")
       			//->and->notEqualTo("h.hoteles_url","")
       			//->and->notEqualTo("h.orbitz_url","")
       			//->and->notEqualTo("h.expedia_url","")
       			->and->notEqualTo("h.mapa","")
       			->and->notEqualTo("h.puntaje","");

		$select->order("h.puntaje desc");

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function searchCity($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array("h"=>$this->table));
		$select->join(array("c"=>"tb_ciudad"),"h.id_ciudad = c.id_ciudad");
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais",array("pais"=>"Name"));

		$select->where
       		->nest
       		    ->and->equalTo('h.id_ciudad', $data['id_ciudad'])
      			->and->equalTo('h.estado', 0)
       			//->and->notEqualTo("h.despegar_url","")
       			->and->notEqualTo("h.booking_url","")
       			//->and->notEqualTo("h.hoteles_url","")
       			//->and->notEqualTo("h.orbitz_url","")
       			//->and->notEqualTo("h.expedia_url","")
       			->and->notEqualTo("h.mapa","")
       			->and->notEqualTo("h.puntaje","");

		if($data['estrellas']!=""){

			if($data['estrellas']=="estrellas1"){
				$select->order("h.calificacion desc");
			}else{
				$select->order("h.calificacion asc");
			}

		}

		if($data['puntaje']!=""){

			if($data['puntaje']=="mayor"){
				$select->order("h.puntaje desc");
			}else{
				$select->order("h.puntaje asc");
			}

		}else{
			$select->order("h.puntaje desc");
		}
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function searchCityFiltro($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array("h"=>$this->table));
		$select->join(array("c"=>"tb_ciudad"),"h.id_ciudad = c.id_ciudad");
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais",array("pais"=>"Name"));

		$select->where
       		->nest
       		    ->and->equalTo('h.id_ciudad', $data['id_ciudad'])
      			->and->equalTo('h.estado', 0)
       			//->and->notEqualTo("h.despegar_url","")
       			->and->notEqualTo("h.booking_url","")
       			//->and->notEqualTo("h.hoteles_url","")
       			//->and->notEqualTo("h.orbitz_url","")
       			//->and->notEqualTo("h.expedia_url","")
       			->and->notEqualTo("h.mapa","")
       			->and->notEqualTo("h.puntaje","");

		if($data['estrellas']!=""){

			if($data['estrellas']=="estrellas1"){
				$select->order("h.calificacion desc");
			}else{
				$select->order("h.calificacion asc");
			}

		}

		if($data['puntaje']!=""){

			if($data['puntaje']=="mayor"){
				$select->order("h.puntaje desc");
			}else{
				$select->order("h.puntaje asc");
			}

		}else{
			$select->order("h.puntaje desc");
		}


		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function searchDistrict($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array("h"=>$this->table));
		$select->join(array("c"=>"tb_ciudad"),"h.id_ciudad = c.id_ciudad");
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais",array("pais"=>"Name"));

		$select->where
			->nest
			->and->equalTo('h.localidad', $data['localidad'])
			->and->equalTo('h.estado', 0)
			//->and->notEqualTo("h.despegar_url","")
			->and->notEqualTo("h.booking_url","")
			//->and->notEqualTo("h.hoteles_url","")
			//->and->notEqualTo("h.orbitz_url","")
			//->and->notEqualTo("h.expedia_url","")
			->and->notEqualTo("h.mapa","")
			->and->notEqualTo("h.puntaje","");

		if($data['estrellas']!=""){

			if($data['estrellas']=="estrellas1"){
				$select->order("h.calificacion desc");
			}else{
				$select->order("h.calificacion asc");
			}

		}

		if($data['puntaje']!=""){

			if($data['puntaje']=="mayor"){
				$select->order("h.puntaje desc");
			}else{
				$select->order("h.puntaje asc");
			}

		}else{
			$select->order("h.puntaje desc");
		}
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function search($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array("h"=>$this->table));
		$select->join(array("c"=>"tb_ciudad"),"h.id_ciudad = c.id_ciudad");
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais",array("pais"=>"Name"));

		/*$select->where(array( 
			new Predicate\Like('h.nombre', '%'.$data.'%'),
			new Predicate\Like('h.localidad', '%'.$data.'%'),
			new Predicate\Like('c.Name', '%'.$data.'%'),
			new Predicate\Like('p.Name', '%'.$data.'%')
			),Predicate\PredicateSet::COMBINED_BY_OR);*/

		$select->where
       		->nest
	            ->like("h.nombre","%".$data."%")
				->or->like("h.localidad","%".$data."%")
			   	->or->like("c.Name","%".$data."%")
			   	->or->like("p.Name","%".$data."%")
       		->unnest
      			->and->equalTo('h.estado', 0)
       			//->and->notEqualTo("h.despegar_url","")
       			->and->notEqualTo("h.booking_url","")
       			//->and->notEqualTo("h.hoteles_url","")
       			//->and->notEqualTo("h.orbitz_url","")
       			//->and->notEqualTo("h.expedia_url","")
       			->and->notEqualTo("h.mapa","")
       			->and->notEqualTo("h.puntaje","");


		$select->order("h.puntaje desc");

        //Permite ver la consulta real
		//echo $select->getSqlString();

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getHotelSimilares($data){

		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array("h"=>$this->table));
		$select->join(array("c"=>"tb_ciudad"),"h.id_ciudad = c.id_ciudad");
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais");

		//$select->where(array("c.id_ciudad"=>$data[0]['id_ciudad']));	

		$select->where(array(new Predicate\Expression('h.id_hotel NOT IN (?)',
                array($data[0]['id_hotel']))));   


		$select->where
		        ->nest
		        ->and->equalTo("c.id_ciudad",$data[0]['id_ciudad'])
		        ->and->notEqualTo("h.booking_url","")
       			//->and->notEqualTo("h.hoteles_url","")
       			//->and->notEqualTo("h.orbitz_url","")
       			//->and->notEqualTo("h.expedia_url","")
       			->and->notEqualTo("h.mapa","")
       			->and->notEqualTo("h.puntaje","");

		$select->order(array("h.calificacion desc","h.puntaje desc"));

		$select->limit(8);

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;

	}

}