<?php

namespace Front\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class FotoTable extends AbstractTableGateway {

	protected $table = 'tb_foto';

	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}

	public function add($datos = array()){
		$this->insert($datos);	
	}

	public function deleteFoto($id) {
        $this->delete(array("id_foto" => $id));
    }

    public function getFoto($idHotel){

    	$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("f" => $this->table));
		$select->join(array("fh"=> "tb_foto_hotel"),"f.id_foto = fh.id_foto");
		$select->join(array("h"=>"tb_hotel"),"fh.id_hotel = h.id_hotel");

		$select->where(array("fh.id_hotel"=>$idHotel));
		//$select->limit(1);

	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();
	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
    }

}

?>