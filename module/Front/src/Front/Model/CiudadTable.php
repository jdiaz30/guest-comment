<?php

namespace Front\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class CiudadTable extends AbstractTableGateway {

	protected $table = 'tb_ciudad';

	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}

    public function getCiudad($data){

    	$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("c" => $this->table));
		$select->columns(array(
			"ciudad"=>"Name",
			"id_ciudad"
		));

		$select->join(array("p"=> "tb_pais"),"c.id_pais = p.id_pais");

		$select->where(array("c.id_pais"=>$data,"c.estado"=>"0"));

		$select->order("ciudad asc");
		
	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();
	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
    }

}

?>