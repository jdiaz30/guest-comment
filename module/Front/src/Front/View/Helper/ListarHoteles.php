<?php

namespace Front\View\Helper;


use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;
use Zend\Session\Container;

use Front\Plugin\ComentarioPlugin;
use Front\Model\FotoTable;
use Front\Model\HotelTable;
use Front\Plugin\MapaPlugin;

class ListarHoteles extends AbstractHelper{

    private $dbAdapter;
    protected $serviceLocator;
    protected $hotelTable;
    protected $fotoHotel;

public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');

        $this->hotelTable = new HotelTable($this->dbAdapter);
        $this->fotoHotel = new FotoTable($this->dbAdapter);
}

/*param es una variable resumida para insertarla en la sesion de busquedas*/

public function ListarHoteles($texto,$param,$tipo){

    $hoteles = $this->tipoConsulta($tipo,$texto);

    $dataHotel = $this->getDataHotel($hoteles);
    //Guardamos los datos en una session con el parametro de busqueda para que no se crucen con otras busquedas
    $hotelSession = new Container($param);

    $hotelSession->hotel = $dataHotel;

    $htmlResponse = $this->htmlHotelSearch($hotelSession->hotel);

    return $htmlResponse;
}

public function ListarHotelesCiudad($data,$tipo){
      
    $hoteles = $this->tipoConsulta($tipo,$data);

    $dataHotel = $this->getDataHotel($hoteles);

    $htmlResponse = $this->htmlHotelSearch($dataHotel);

    return $htmlResponse;
}

public function getDataHotel($hoteles){

    $dataHotel = array();

    $i = 0;

    foreach ($hoteles as $data ) {
        //if($data['puntaje']!=""){
            //$foto = $this->fotoHotel->getFoto($data['id_hotel'])->toArray();

            $dataHotel[$i]['nombre'] = $data['nombre'];
            $dataHotel[$i]['puntaje'] = $data['puntaje'];
            $dataHotel[$i]['calificacion'] = $data['calificacion'];
            $dataHotel[$i]['descripcion'] = $data['descripcion'];
            $dataHotel[$i]['mapa'] = $data['mapa'];
            $dataHotel[$i]['url'] = $data['url'];
            $dataHotel[$i]['id'] = $data['id_hotel'];
            $dataHotel[$i]['direccion'] = $data['direccion'];
            $dataHotel[$i]['localidad'] = $data['localidad'];
            $dataHotel[$i]['ciudad'] = $data['Name'];
            $dataHotel[$i]['pais'] = $data['pais'];
            $dataHotel[$i]['booking'] = $data['booking_url'];
            $dataHotel[$i]['hoteles'] = $data['hoteles_url'];
            $dataHotel[$i]['expedia'] = $data['expedia_url'];
            $dataHotel[$i]['despegar'] = $data['despegar_url'];
            $dataHotel[$i]['orbitz'] = $data['orbitz_url'];
            
            
            //$dataHotel[$i]['foto'] = "hotel1.jpg";
            
            $i +=1;
        //}

    }

    return $dataHotel;
}

public function htmlHotelSearch($hotelSession,$puntaje=null,$estrella=null){

    $htmlResponse = "";
    $datosHotel = array();
    $mapaPlugin = new MapaPlugin();

    if(isset($puntaje) || isset($estrella)){
      $datosHotel = $this->ordenarHoteles($hotelSession,$puntaje,$estrella);
    }else{
      $datosHotel = $hotelSession;
    }

    $i=1;

    foreach ($datosHotel as $data) {
        $mapa = array();
        if($data['mapa']!=""){
           $mapa = $mapaPlugin->obtenerCoordenadas($data);
        }

        //$foto = '/img/hotel1.jpg';
        $htmlResponse .='<div class="row no box-hotel-item">';

        $htmlResponse .='<div class="col-xs-9 no">';
        $htmlResponse .='<div class="detalle-hotel">';

        $htmlResponse .='<div id="myCarousel'.$i.'" class="carousel slide" data-ride="carousel" data-interval="false">';

        $htmlResponse .='<div class="carousel-inner" role="listbox">';

        $fotoData = $this->fotoHotel->getFoto($data['id']);

        $totalFoto = count($fotoData);

        if($totalFoto>0){

            $j=0;
          
            foreach ($fotoData as $datos) {

                $foto = MEDIA_URL."/hoteles/".$datos['foto'];

                if($j==0){
                    $htmlResponse .='<div class="item active image">
                    <img src='.$foto.'>
                    </div>';

                }else{

                    $htmlResponse .='<div class="item image">
                    <img src='.$foto.'>
                    </div>';
                }

                $j +=1;

            }

        }else{

            $foto = MEDIA_URL."/hoteles/hotel1.jpg";

            $htmlResponse .='<div class="item image">
            <img src='.$foto.'>
            </div>';
  
        }

        $htmlResponse .='</div>';//carousel-inner

        if($totalFoto>1){

            $htmlResponse .='<a class="left carousel-control" href="#myCarousel'.$i.'" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>';

            $htmlResponse .='<a class="right carousel-control" href="#myCarousel'.$i.'" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>';

        }

        $htmlResponse .='</div>';//carousel

        $htmlResponse .='<div class="detalle border-right">
        <div class="box-head-hotel">';

        $htmlResponse .='<div class="puntaje" id="promedio">'.$data['puntaje'].'</div>';

        $htmlResponse .='<div class="box-name-hotel">';
        $htmlResponse .='<div class="name-hotel">';

        $htmlResponse .='<a href="/hotel/name/'.$data['url'].'">'.$data['nombre'].'</a>';

        $calificacion = $this->getCalificacion($data['calificacion']);

        $htmlResponse .='<div class="star">'.$calificacion.'</div>';

        $htmlResponse .='</div>';//name-hotel

        $htmlResponse .='<div class="box-direccion">';

        $direccion = $this->generaDireccion($data['direccion'],$data['localidad'],$data['ciudad'],$data['pais']);

        $htmlResponse .='<i class="item-mapa"></i>';
        $htmlResponse .='<span class="direccion">'.$direccion.'</span>';
        $htmlResponse .=' <a href="javascript:void(0)"  class="mapaH" data-longitud="'.$mapa[1].'" data-latitud="'.$mapa[0].'" data-zoom="'.$mapa[2].'" data-name="'.$data['nombre'].'">Ver mapa</a>
       ';
  
        $htmlResponse .='</div>';//box-direccion

        $htmlResponse .='</div>';//box-name-hotel

        $htmlResponse .='</div>';//box-head-hotel

        $htmlResponse .='<div class="texto"><p>'.substr($data["descripcion"],0,250).' ...'.'</p></div>';

        $htmlResponse .='<div class="icons-servicios">
        <img src="/img/icons-hotel.jpg">
        </div>';

        $htmlResponse .=' <div class="box-btn-comentario">
        <a class="icon-btn" href="/hotel/name/'.$data['url'].'">
                   <i class="x-caret"></i>
        </a>
        <a href="/hotel/name/'.$data['url'].'" class="btn-comentario">Ver Comentarios</a>
        </div>';

        $htmlResponse .='</div>';//detalle 


        $htmlResponse .='</div>';// detalle-hotel
        $htmlResponse .='</div>';// col-xs-9 no

        $htmlResponse .='<div class="col-xs-3">';

        $htmlResponse .='<div class="box-motor-item">';

        $htmlResponse .='<div class="row no">
        <div class="t-precio">Los mejores precios para tu estadía.</div>
        </div>';

        $htmlResponse .='<div class="row no">
        <div class="box-calendar">';

        $htmlResponse .='<div class="input-daterange input-group" id="datepicker">
        
         <input type="text" data-beatpicker="true" data-beatpicker-module="clear,icon,footer" data-beatpicker-range="true" data-beatpicker-id="beatpicker'.$i.'" item='.$i.'>

        </div>';

        $htmlResponse .='<div class="row no">
        <a href="javascript:void(0)" class="btn-precio" 
        booking="'.$data['booking'].'" hoteles="'.$data['hoteles'].'" orbitz="'.$data['orbitz'].'" expedia="'.$data['expedia'].'" item='.$i.' >Mostrar precios</a>
        </div>';

        $htmlResponse .='</div>';//row no 

        $htmlResponse .='</div>';//box-calendar

        $htmlResponse .='</div>';//box-motor-item

        $htmlResponse .='</div>';//col-xs-3


        $htmlResponse .='</div>';//box-hotel-item

        $i +=1;

    }

    return $htmlResponse;
}

public function tipoConsulta($tipo,$data){

    $hoteles = "";

    switch ($tipo) {
        case 'todos':
            $hoteles = $this->hotelTable->search($data);
            break;

        case 'country':
            $hoteles = $this->hotelTable->searchCountry($data);
            break;

        case 'city':
            $hoteles = $this->hotelTable->searchCity($data);
            break;

        case 'city-filtro-pais':
            $hoteles = $this->hotelTable->searchCityFiltro($data);
            break;

        case 'district':
            $hoteles = $this->hotelTable->searchDistrict($data);
            break;

        
        default:
            $hoteles = $this->hotelTable->search($data);
            break;
    }

    return $hoteles;
}

public function generaDireccion($direccion,$localidad,$ciudad,$pais){
    $newDireccion = "";

    if($localidad == ""){
       $newDireccion = $direccion.",".$ciudad.",".$pais;
    }else{
       $newDireccion = $direccion.",".$localidad.",".$pais;
    }

    return $newDireccion;

}

public function getCalificacion($calificacion){
    $htmlResponse = "";

    for ($i=1; $i <= $calificacion; $i++) { 
        $htmlResponse .='<i class="item-star"></i>';
    }

    return $htmlResponse;
}

public function ordenarHoteles($hotelData,$puntaje,$estrella){
    $ordenar = "";

    if(isset($puntaje) && !empty($puntaje)){
        if($puntaje == "mayor"){
            $ordenar = create_function(
            '$a, $b', 'return $b["puntaje"] - $a["puntaje"];'
            );
        }else{
             $ordenar = create_function(
            '$a, $b', 'return $a["puntaje"] - $b["puntaje"];'
            );
        }
   
       usort($hotelData,$ordenar);
    }

    if(isset($estrella) && !empty($estrella)){

      if($estrella == "estrellas1"){
        $ordenar = create_function(
            '$a, $b', 'return $b["calificacion"] - $a["calificacion"];'
        );

    }else{
        $ordenar = create_function(
            '$a, $b', 'return $a["calificacion"] - $b["calificacion"];'
        );

    }
        usort($hotelData,$ordenar);
    }

    return $hotelData;
}

public function getFotoHotel($idHotel){

}

public function calculaPuntaje($data){
    $totalData = count($data);
    $total = 0;

    if($totalData>0){
        foreach ($data as $datos) {
            $total+=(float)$datos['puntaje'];    
        }

        $promedio = $total / $totalData;
        $promedio = number_format($promedio, 1, ',', ' ');

    }else{
        $promedio = 0;
    }

    return $promedio;
}


}

?>
