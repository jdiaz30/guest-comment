<?php

namespace Front\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

use Front\Model\HotelTable;

class CrearTitleSubMenu extends AbstractHelper{

	private $dbAdapter;
    protected $serviceLocator;
    protected $hotelTable;

	public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');

        $this->hotelTable = new HotelTable($this->dbAdapter);
    }
    public function subMenuTituloHotel($hotelUrl){

    	$datos = $this->hotelTable->getAllUrl($hotelUrl);

        $urlPais = URL."/country/search/".$datos['pais']."/".$datos['id_pais'];
        $urlCiudad = URL."/city/search/".$datos['ciudad']."/".$datos['id_ciudad'];
        $urlDistrito = URL."/district/search/".$datos['localidad'];
        
        $htmlResponse = "<li><a href='javascript:void(0)'> - </a></li>";
    	$htmlResponse .= "<li><a href='javascript:void(0)' class=''>".$datos['Continent']."</a></li>";
        $htmlResponse .= "<li><a href='javascript:void(0)'> - </a></li>";
    	$htmlResponse .= "<li><a href=".$urlPais." class='si'>".$datos['pais']."</a></li>";
        $htmlResponse .= "<li><a href='javascript:void(0)'> - </a></li>";
    	$htmlResponse .= "<li><a href=".$urlCiudad." class='si'>".$datos['ciudad']."</a></li>";
        $htmlResponse .= "<li><a href='javascript:void(0)'> - </a></li>";
        $htmlResponse .= "<li><a href=".$urlDistrito." class='si negrita'>".$datos['localidad']."</a></li>";

    	return $htmlResponse;

    }

}

?>