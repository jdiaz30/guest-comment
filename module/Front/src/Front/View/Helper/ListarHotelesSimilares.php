<?php

namespace Front\View\Helper;


use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;
use Zend\Session\Container;

use Front\Plugin\ComentarioPlugin;
use Front\Model\FotoTable;
use Front\Model\HotelTable;

class ListarHotelesSimilares extends AbstractHelper{

    private $dbAdapter;
    protected $serviceLocator;
    protected $hotelTable;
    protected $fotoHotel;

public function __construct(ServiceLocator $serviceLocator){
    $this->serviceLocator = $serviceLocator;
    $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');

    $this->hotelTable = new HotelTable($this->dbAdapter);
    $this->fotoHotel = new FotoTable($this->dbAdapter);
}

public function listarHoteles($ubicacion){

    $hotelData = $this->hotelTable->getHotelSimilares($ubicacion);

    $htmlResponse = "";

    foreach ($hotelData as $data) {
        $fotoData = $this->fotoHotel->getFoto($data['id_hotel'])->toArray();

        $foto = MEDIA_URL."/hoteles/".$fotoData[0]['foto'];
        $url = URL."/hotel/name/".$data['url'];

        $calificacion = $this->getCalificacion($data['calificacion']);

        $htmlResponse .= '<div class="row no item-hotel-recomend">';
       
        $htmlResponse .= '<div class="foto"><a href="'.$url.'"><img src="'.$foto.'"></a></div>';
        
        $htmlResponse .= '<div class="detalle">';
        $htmlResponse .= '<div class="name-h"><a href="'.$url.'">'.$data['nombre'].'</a></div>';

        $htmlResponse .= '<div class="star-h">';
        $htmlResponse .= $calificacion;
        $htmlResponse .= '</div>';//star-h

        $htmlResponse .= '</div>';//detalle

        $htmlResponse .= '</div>';//item-hotel-recomend
    }

    return $htmlResponse;

}

public function getCalificacion($calificacion){

    $htmlResponse = "";

    for ($i=1; $i <= 5; $i++) { 
        if($i<=$calificacion){
             $htmlResponse .='<i class="item-star"></i>';
        }else{
            $htmlResponse .='<i class="item-star-no"></i>';
          
        }
        
    }

    return $htmlResponse;

}









}

?>
