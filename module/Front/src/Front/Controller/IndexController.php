<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */


namespace Front\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
class IndexController extends AbstractActionController {

    protected $hotelTable;

    public function getHotelTable(){
        if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Front\Model\HotelTable');
        }
        return $this->hotelTable;
    }

    public function indexAction() {

        $this->dataPlugin()->updateDataFiltros();
        
    }

    public function changeLanguageAction(){
    		$language=$this->getRequest()->getPost('language');

    		$languageSession = new Container('language');
    		$languageSession->language=$language;
           
           //echo var_dump($languageSession->language);

            //Establecemos el idioma en el traductor
            $translator = $this->getServiceLocator()->get('translator');
            $translator->setLocale($languageSession->language)->setFallbackLocale($languageSession->language);

    		return $this->getResponse()->setContent(0);
    }


   /* public function filtrarHotelAction(){

        $nombre = $this->getRequest()->getPost('q');

        $data = $this->getHotelTable()->getHotelFiltro($nombre);
    
        return $this->getResponse()->setContent(json_encode($data->toArray())); 
    }

    public function filtrarLocalidadAction(){

        $nombre = $this->getRequest()->getPost('q');

        $data = $this->getHotelTable()->getLocalidadFiltro($nombre);
    
        return $this->getResponse()->setContent(json_encode($data->toArray())); 
    }

    public function filtrarCiudadAction(){

        $nombre = $this->getRequest()->getPost('q');

        $data = $this->getHotelTable()->getCiudadFiltro($nombre);
    
        return $this->getResponse()->setContent(json_encode($data->toArray())); 
    }

    public function filtrarPaisAction(){

        $nombre = $this->getRequest()->getPost('q');

        $data = $this->getHotelTable()->getPaisFiltro($nombre);
    
        return $this->getResponse()->setContent(json_encode($data->toArray())); 
    }*/

}
