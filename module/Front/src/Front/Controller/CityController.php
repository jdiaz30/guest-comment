<?php

namespace Front\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Front\View\Helper\ListarHoteles;

class CityController extends AbstractActionController {

	protected $hotelTable;

	public function getHotelTable(){
		if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Front\Model\HotelTable');
        }
        return $this->hotelTable;
	}

  
    public function indexAction(){
       
        return $this->redirect()->toUrl('/');

    }

    public function searchAction(){
        $texto = $this->getEvent()->getRouteMatch()->getParam('city');
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        
        if($texto == "" || $id == ""){
            return $this->redirect()->toUrl('/');
        }

        return new ViewModel(array(
            'texto'=>$texto,
            'param'=> $id,
         
         
        ));
    }

    public function searchHotelAction(){

        $viewHoteles = new ListarHoteles($this->getServiceLocator());

        $data = $this->getRequest()->getPost();

        $hotel = $viewHoteles->ListarHotelesCiudad($data,"city");

        return $this->getResponse()->setContent($hotel);
 
    }

 

 

    



}

?>