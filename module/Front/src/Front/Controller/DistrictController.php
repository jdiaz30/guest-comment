<?php

namespace Front\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Front\View\Helper\ListarHoteles;

class DistrictController extends AbstractActionController {

	protected $hotelTable;
   
	public function getHotelTable(){
		if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Front\Model\HotelTable');
        }
        return $this->hotelTable;
	}

  
    public function indexAction(){
    
         return $this->redirect()->toUrl('/');
       
    }

    public function searchAction(){
        $texto = $this->getEvent()->getRouteMatch()->getParam('district');
        
        if($texto == ""){
            return $this->redirect()->toUrl('/');
        }

        return new ViewModel(array(
            'texto'=>$texto,
         
         
        ));
    }

    public function searchHotelAction(){

        $viewHoteles = new ListarHoteles($this->getServiceLocator());

        $data = $this->getRequest()->getPost();

        $hotel = $viewHoteles->ListarHotelesCiudad($data,"district");

        return $this->getResponse()->setContent($hotel);
 
    }

 

 

    



}

?>