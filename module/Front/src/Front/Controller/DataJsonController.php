<?php

namespace Front\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class DataJsonController extends AbstractActionController {

	public function dataHotelFiltrosAction(){

		$data = $this->motorReservasPlugin()->curlUrl(URL_DATA."/data/data-hotel.json",false);

		return $this->getResponse()->setContent($data);

	}

	public function dataCiudadFiltrosAction(){

		$data = $this->motorReservasPlugin()->curlUrl(URL_DATA."/data/data-ciudad.json",false);

		return $this->getResponse()->setContent($data);

	}

	public function dataPaisFiltrosAction(){

		$data = $this->motorReservasPlugin()->curlUrl(URL_DATA."/data/data-pais.json",false);

		return $this->getResponse()->setContent($data);

	}

	public function dataLocalidadFiltrosAction(){

		$data = $this->motorReservasPlugin()->curlUrl(URL_DATA."/data/data-localidad.json",false);

		return $this->getResponse()->setContent($data);

	}
}

