<?php

namespace Front\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Front\View\Helper\ListarHoteles;

class SearchController extends AbstractActionController {

	protected $hotelTable;

	public function getHotelTable(){
		if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Front\Model\HotelTable');
        }
        return $this->hotelTable;
	}

	public function indexAction(){

		$data = $this->getEvent()->getRouteMatch()->getParam('p');
		$texto = urldecode($data);
		$posicion = strpos($texto, ",");

		if($posicion == true){
		   $eliminar = substr($texto,$posicion);
		   $texto =str_replace($eliminar,"",$texto);
		}
		
		$s = strtoupper(md5(uniqid(rand(),true))); 
		$param = substr($data,0,1);
		$param =  $param.substr($s,0,2);
   
  	    return new ViewModel(array(
  	    	'texto'=>$texto,
  	    	'param'=>$param
  	    ));
		
	}

	public function searchHotelAction(){

		$viewHoteles = new ListarHoteles($this->getServiceLocator());

		$data = $this->getRequest()->getPost();

		$hotel = $viewHoteles->ListarHoteles($data['texto'],$data['param'],"todos");

		return $this->getResponse()->setContent($hotel);
 
	}

	public function filtrarHotelAction(){

		$data = $this->getRequest()->getPost();
		$viewHoteles = new ListarHoteles($this->getServiceLocator());

		$hotelSession = new Container($data['param']);

		$hoteles = $viewHoteles->htmlHotelSearch($hotelSession->hotel,$data['puntaje'],$data['estrella']);

		return $this->getResponse()->setContent($hoteles);

	}


}

