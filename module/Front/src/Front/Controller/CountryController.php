<?php

namespace Front\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Front\View\Helper\ListarHoteles;

class CountryController extends AbstractActionController {

	protected $hotelTable;
    protected $ciudadTable;

	public function getHotelTable(){
		if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Front\Model\HotelTable');
        }
        return $this->hotelTable;
	}

    public function getCiudadTable(){
        if (!$this->ciudadTable) {
            $sm = $this->getServiceLocator();
            $this->ciudadTable = $sm->get('Front\Model\CiudadTable');
        }
        return $this->ciudadTable;
    }

    public function indexAction(){

            return $this->redirect()->toUrl('/');
       
    }

    public function searchAction(){
        $texto = $this->getEvent()->getRouteMatch()->getParam('country');
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        
        if($texto == "" || $id == ""){
            return $this->redirect()->toUrl('/');
        }

        $ciudad = $this->getCiudadTable()->getCiudad($id);

        return new ViewModel(array(
            'texto'=>$texto,
            'param'=> $id,
            'ciudad'=>$ciudad
         
        ));
    }

    public function searchHotelAction(){

        $viewHoteles = new ListarHoteles($this->getServiceLocator());

        $data = $this->getRequest()->getPost();

        $hotel = $viewHoteles->ListarHoteles($data['param'],$data['param'],"country");

        return $this->getResponse()->setContent($hotel);
 
    }

    public function filtrarHotelCiudadAction(){

        $viewHoteles = new ListarHoteles($this->getServiceLocator());

        $data = $this->getRequest()->getPost();

        if($data['id_ciudad']==""){
            $hotelSession = new Container($data['param']);
            $hoteles = $viewHoteles->htmlHotelSearch($hotelSession->hotel,$data['puntaje'],$data['estrellas']);
        }else{
            $hoteles = $viewHoteles->ListarHotelesCiudad($data,"city-filtro-pais");
        }

        return $this->getResponse()->setContent($hoteles);
 
    }

    



}

?>