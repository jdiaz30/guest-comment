<?php

namespace Front\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

use Front\View\Helper\CrearTitleSubMenu;
use Front\View\Helper\ListarHotelesSimilares;

class HotelController extends AbstractActionController {

	protected $hotelTable;
    protected $fotoHotelTable;

	public function getHotelTable(){
		if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Front\Model\HotelTable');
        }
        return $this->hotelTable;
	}

    public function getFotoHotelTable(){
        if (!$this->fotoHotelTable) {
            $sm = $this->getServiceLocator();
            $this->fotoHotelTable = $sm->get('Front\Model\FotoHotelTable');
        }
        return $this->fotoHotelTable;
    }

    public function indexAction() {
      
        $url=$this->getEvent()->getRouteMatch()->getParam('name');
        $tituloHelperView = new CrearTitleSubMenu($this->getServiceLocator());
     
        $subMenu = $tituloHelperView->subMenuTituloHotel($url);

        $hotel = $this->getHotelTable()->getAllUrl($url);

        $xHotel = array($hotel);
        $foto = $this->getFotoHotelTable()->getAllHotel($xHotel[0]['id_hotel']);

        //Capturamos los valores del mapa
        $mapa = $this->mapaPlugin()->obtenerCoordenadas($hotel);
        //$mapa = explode(",", $mapa);

        //Creamos un id para las sesiones de los comentarios del hotel
        $s = strtoupper(md5(uniqid(rand(),true))); 
        $param = substr($url,0,1);
        $param =  $param.substr($s,0,3);

        //Mostramos hoteles similares

        $hotelSimilarView = new ListarHotelesSimilares($this->getServiceLocator());
        $d = array($hotel);
        $hotelSimilarData = $hotelSimilarView->listarHoteles($d);
      
        return new ViewModel(array(
            'url'=>$url,
            'hotel'=>$hotel,
            'subMenu'=>$subMenu,
            'mapa'=>$mapa,
            'param'=>$param,
            'hotelSimilar'=>$hotelSimilarData,
            'foto'=>$foto,
            'bookingUrl'=>$xHotel[0]['booking_url'],
            'despegarUrl'=>$xHotel[0]['despegar_url'],
            'hotelesUrl'=>$xHotel[0]['hoteles_url'],
            'orbitzUrl'=>$xHotel[0]['orbitz_url'],
            'expediaUrl'=>$xHotel[0]['expedia_url']
            ));
    }

    public function listCommentAction(){
        $data = $this->getRequest()->getPost();

        $hotelComment = $this->getHotelTable()->getAllUrl($data['url']);

        $comentarios = $this->comentarioPlugin()->capturaComentarios(
                $data['hotel-id'],
                $data['idioma'],
                $hotelComment['booking_url'],
                $hotelComment['despegar_url'],
                $hotelComment['hoteles_url'],
                $hotelComment['orbitz_url'],
                $hotelComment['expedia_url'],
                0,
                10,$this->getServiceLocator()->get('translator'));

        return $this->getResponse()->setContent($comentarios);
    }

    public function promedioCommentAction(){
        $data = $this->getRequest()->getPost();
        
        $promedio = $this->promedioPlugin()->calculaPromedio($data);

        return $this->getResponse()->setContent($promedio);
    }

    public function listCommentPaginadorAction(){

        $data=$this->getRequest()->getPost();
        $comentarioSession=new Container("co".$data['hotel-id']);

        $comentarios = $this->ComentarioPlugin()->mostrarComentario(
                $comentarioSession,
                $data['idioma'],
                $data['paginador'],
                $data['limit'],
                $this->getServiceLocator()->get('translator'));

        return $this->getResponse()->setContent($comentarios);
    }

    public function motorReservaAction(){

        $data = $this->getRequest()->getPost();

        $response = $this->motorReservasPlugin()->verDisponibilidad($data);

        return $this->getResponse()->setContent($response);
    }


}

?>