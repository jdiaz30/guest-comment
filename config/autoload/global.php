<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */


 if (!defined('MEDIA_URL'))
            define('MEDIA_URL', "http://admine.guest-comment.com/img");


 if (!defined('URL'))
            define('URL', "http://guest-comment.com");

 if (!defined('URL_DATA'))
            define('URL_DATA', "http://admine.guest-comment.com");

return array(

      'service_manager' => array(
        'factories' => array(
            'Zend\Log\Logger' => function($sm){
                $logger = new Zend\Log\Logger;
                $writer = new Zend\Log\Writer\Stream('././data/log/'.date('Y-m-d').'-error.log');

                $logger->addWriter($writer);

                return $logger;
            },
        ),
    ),

    //Definimos los accesos para la base de datos
    'db' => array(
        'driver' => 'Mysqli',
        'database' => 'admin_guest',
        'username' => 'jejej',
        'password' => 'eewew',
        'charset' => 'utf8',
        'options' => array('buffer_results' => true)
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
            => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);
